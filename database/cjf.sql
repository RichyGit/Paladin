/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50539
Source Host           : localhost:3306
Source Database       : cjf4

Target Server Type    : MYSQL
Target Server Version : 50539
File Encoding         : 65001

Date: 2016-04-29 00:12:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cnvp_action
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_action`;
CREATE TABLE `cnvp_action` (
  `logic_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cnvp_action
-- ----------------------------

-- ----------------------------
-- Table structure for cnvp_bumb
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_bumb`;
CREATE TABLE `cnvp_bumb` (
  `id` int(11) NOT NULL DEFAULT '0',
  `price` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of cnvp_bumb
-- ----------------------------
INSERT INTO `cnvp_bumb` VALUES ('1', '23', 'aa');

-- ----------------------------
-- Table structure for cnvp_device
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_device`;
CREATE TABLE `cnvp_device` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `command_id` int(11) DEFAULT NULL,
  `command_type` varchar(255) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cnvp_device
-- ----------------------------

-- ----------------------------
-- Table structure for cnvp_device1
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_device1`;
CREATE TABLE `cnvp_device1` (
  `id` varchar(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `temp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cnvp_device1
-- ----------------------------
INSERT INTO `cnvp_device1` VALUES ('1', 'asdd', '22');
INSERT INTO `cnvp_device1` VALUES ('2', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('3', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('4', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('5', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('6', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('7', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('8', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('9', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('10', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('11', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('12', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('13', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('14', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('15', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('16', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('17', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('18', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('19', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('20', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('21', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('22', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('23', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('24', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('25', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('26', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('27', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('28', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('29', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('30', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('31', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('32', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('33', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('34', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('35', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('36', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('37', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('38', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('39', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('40', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('41', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('42', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('43', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('44', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('45', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('46', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('47', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('48', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('49', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('50', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('51', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('52', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('53', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('54', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('55', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('56', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('57', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('58', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('59', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('60', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('61', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('62', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('63', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('64', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('65', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('66', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('67', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('68', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('69', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('70', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('71', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('72', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('73', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('74', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('75', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('76', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('77', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('78', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('79', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('80', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('81', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('82', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('83', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('84', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('85', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('86', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('87', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('88', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('89', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('90', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('91', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('92', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('93', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('94', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('95', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('96', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('97', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('98', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('99', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('100', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('101', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('102', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('103', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('104', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('105', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('106', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('107', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('108', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('109', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('110', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('111', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('112', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('113', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('114', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('115', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('116', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('117', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('118', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('119', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('120', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('121', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('122', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('123', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('124', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('125', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('126', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('127', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('128', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('129', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('130', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('131', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('132', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('133', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('134', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('135', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('136', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('137', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('138', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('139', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('140', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('141', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('142', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('143', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('144', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('145', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('146', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('147', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('148', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('149', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('150', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('151', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('152', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('153', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('154', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('155', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('156', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('157', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('158', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('159', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('160', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('161', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('162', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('163', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('164', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('165', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('166', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('167', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('168', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('169', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('170', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('171', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('172', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('173', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('174', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('175', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('176', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('177', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('178', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('179', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('180', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('181', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('182', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('183', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('184', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('185', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('186', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('187', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('188', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('189', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('190', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('191', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('192', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('193', 'aaa', '236');
INSERT INTO `cnvp_device1` VALUES ('194', 'aaa', '236');

-- ----------------------------
-- Table structure for cnvp_device2
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_device2`;
CREATE TABLE `cnvp_device2` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `light` varchar(255) DEFAULT NULL,
  `temp` int(255) DEFAULT NULL,
  `rgb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cnvp_device2
-- ----------------------------
INSERT INTO `cnvp_device2` VALUES ('1', 'qaweq', '23', '23', '23');

-- ----------------------------
-- Table structure for cnvp_device3
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_device3`;
CREATE TABLE `cnvp_device3` (
  `price` float DEFAULT NULL,
  `name` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cnvp_device3
-- ----------------------------

-- ----------------------------
-- Table structure for cnvp_device5
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_device5`;
CREATE TABLE `cnvp_device5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float(10,2) DEFAULT NULL,
  `name` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_device5
-- ----------------------------
INSERT INTO `cnvp_device5` VALUES ('1', '12.30', 'adsa');
INSERT INTO `cnvp_device5` VALUES ('2', '12.60', 'cc');

-- ----------------------------
-- Table structure for cnvp_devicecontrol
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_devicecontrol`;
CREATE TABLE `cnvp_devicecontrol` (
  `command_id` int(11) NOT NULL AUTO_INCREMENT,
  `command_type` varchar(255) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`command_id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cnvp_devicecontrol
-- ----------------------------
INSERT INTO `cnvp_devicecontrol` VALUES ('111', 'execute', 'wqe', 'close', '0');
INSERT INTO `cnvp_devicecontrol` VALUES ('112', 'execute', 'wqe', 'open', '100');
INSERT INTO `cnvp_devicecontrol` VALUES ('113', 'execute', '12', 'close', '0');
INSERT INTO `cnvp_devicecontrol` VALUES ('114', 'execute', '11', 'close', '0');
INSERT INTO `cnvp_devicecontrol` VALUES ('115', 'execute', '213', 'close', '0');
INSERT INTO `cnvp_devicecontrol` VALUES ('116', 'execute', 'qwe', 'open', '100');
INSERT INTO `cnvp_devicecontrol` VALUES ('117', 'execute', '6453', 'open', '100');
INSERT INTO `cnvp_devicecontrol` VALUES ('118', 'execute', '24356', 'open', '100');
INSERT INTO `cnvp_devicecontrol` VALUES ('119', 'execute', '7564', 'close', '0');
INSERT INTO `cnvp_devicecontrol` VALUES ('120', 'execute', '43567', 'close', '0');
INSERT INTO `cnvp_devicecontrol` VALUES ('121', 'execute', 'qtr', 'close', '0');
INSERT INTO `cnvp_devicecontrol` VALUES ('122', 'execute', '45436', 'close', '0');
INSERT INTO `cnvp_devicecontrol` VALUES ('123', 'execute', '12', 'open', '100');
INSERT INTO `cnvp_devicecontrol` VALUES ('124', 'execute', '12', 'close', '0');

-- ----------------------------
-- Table structure for cnvp_equipment
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_equipment`;
CREATE TABLE `cnvp_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logic_id` varchar(512) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `devicetype` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `num_in_room` varchar(32) DEFAULT NULL,
  `panel_id` int(11) DEFAULT NULL,
  `room_name` varchar(512) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  `param1` varchar(512) DEFAULT NULL,
  `param2` varchar(512) DEFAULT NULL,
  `param3` varchar(512) DEFAULT NULL,
  `param4` varchar(512) DEFAULT NULL,
  `param5` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `panelFK` (`panel_id`),
  KEY `roomFK` (`room_id`),
  CONSTRAINT `panelFK` FOREIGN KEY (`panel_id`) REFERENCES `cnvp_panel` (`id`),
  CONSTRAINT `roomFK` FOREIGN KEY (`room_id`) REFERENCES `cnvp_room` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_equipment
-- ----------------------------
INSERT INTO `cnvp_equipment` VALUES ('1', '12', '1', '2', 'device1', '1', '2', '1', '1', '', 'close', '1', '1', '1', '1', '1');
INSERT INTO `cnvp_equipment` VALUES ('4', '11', '1', 'aa', 'device2', '1', null, null, null, '', 'close', null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('8', '213', '1', '213', 'device1', '1', '1', '1', '1', '', 'close', '1', '1', '1', '1', '1');
INSERT INTO `cnvp_equipment` VALUES ('9', 'qwe', '1', 'wqe', 'device1', '1', '1', '1', '1', '', 'open', '2', '2', '2', '2', '2');
INSERT INTO `cnvp_equipment` VALUES ('13', '6453', '1', 'rqetw', null, '7654', null, '43356', '1', '', 'open', null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('14', '24356', '1', 'ert', null, null, null, '567', '1', '', 'open', null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('15', '7564', '1', '35', null, null, '1', '456546', '1', '', 'close', null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('16', '43567', '1', '5877989', null, null, '2', '3456', '31', '', 'close', null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('17', 'qtr', '33', 'eqr', null, null, '1', 'asfd', '1', '', 'close', null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('18', '45436', '33', '4356', null, 'rete', '1', '465', '31', '', 'close', '7', '879', '89', '879', '879');
INSERT INTO `cnvp_equipment` VALUES ('19', '654324', '33', 'ytrfdv', null, '52', '1', '546ere', '1', '', 'close', null, '34265', 'erwt', 'etrhhf', '46');
INSERT INTO `cnvp_equipment` VALUES ('29', '1', '1', '1', null, null, '1', '1', '1', '', 'close', '1', '1', '1', '1', '1');
INSERT INTO `cnvp_equipment` VALUES ('30', '1', '1', '1', null, null, '1', '1', '1', '', 'close', '1', '1', '1', '1', '1');
INSERT INTO `cnvp_equipment` VALUES ('31', ' a', '1', 'ada', null, 'device2', '1', ' 234', '1', '', 'close', ' 234', ' 234', ' ', ' 234', ' 234');
INSERT INTO `cnvp_equipment` VALUES ('42', '4556rt', '1', '场景测试1', null, 'RGB', null, null, null, 'value0', 'close', null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('45', '789789', '1', '场景测试2', null, 'RGB', null, null, null, 'value0', 'close', null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('128', '166', '75', 'YW DENG', null, '41', null, null, null, '-1', null, null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('129', '143', '74', 'RGB', null, '40', null, null, null, '-1', null, null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('131', '143', '73', 'RGB', null, '40', null, null, null, '-1', null, null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('135', '144', '73', 'YW灯', null, '41', null, null, null, '客厅', null, null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('143', '168', '72', 'RGB灯', null, '40', null, null, null, '客厅', null, null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('144', '166', '72', 'YW灯', null, '41', null, null, null, '客厅', null, null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('145', '143', '72', 'jkk', null, '40', null, null, null, '-1', null, null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('146', '165', '75', 'RGB finish', null, '40', null, null, null, '-1', null, null, null, null, null, null);
INSERT INTO `cnvp_equipment` VALUES ('147', '165', '82', 'RGBdeng', null, '40', null, null, null, '-1', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for cnvp_message_type
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_message_type`;
CREATE TABLE `cnvp_message_type` (
  `createtime` bigint(11) DEFAULT NULL,
  `del` int(11) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `msgtypename` varchar(256) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_message_type
-- ----------------------------
INSERT INTO `cnvp_message_type` VALUES ('123', '0', '1', 'device1', '1');
INSERT INTO `cnvp_message_type` VALUES ('111', '0', '1', 'device2', '2');
INSERT INTO `cnvp_message_type` VALUES ('201512231215', '0', '1', 'test', '3');
INSERT INTO `cnvp_message_type` VALUES ('201512230308', '0', '1', 'test111', '4');
INSERT INTO `cnvp_message_type` VALUES ('201512230456', '0', '1', 'bumb', '5');

-- ----------------------------
-- Table structure for cnvp_netcontroller
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_netcontroller`;
CREATE TABLE `cnvp_netcontroller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(128) NOT NULL,
  `port` int(32) NOT NULL,
  `main` varchar(512) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `domain` varchar(512) DEFAULT NULL,
  `net_select` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_netcontroller
-- ----------------------------
INSERT INTO `cnvp_netcontroller` VALUES ('1', null, '11.342', '222', 'asdada', 'address', 'domain', 'net_select');

-- ----------------------------
-- Table structure for cnvp_panel
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_panel`;
CREATE TABLE `cnvp_panel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(512) NOT NULL,
  `type` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_panel
-- ----------------------------
INSERT INTO `cnvp_panel` VALUES ('1', null, '2', '2');
INSERT INTO `cnvp_panel` VALUES ('2', null, '66', '666');
INSERT INTO `cnvp_panel` VALUES ('3', null, '温度控制板', 'rwerwewer');
INSERT INTO `cnvp_panel` VALUES ('4', null, '测试', 'sadds');
INSERT INTO `cnvp_panel` VALUES ('5', null, '5', '5');
INSERT INTO `cnvp_panel` VALUES ('7', null, 'aab', 'b');
INSERT INTO `cnvp_panel` VALUES ('9', null, '2', 's');
INSERT INTO `cnvp_panel` VALUES ('10', null, '测试', '测试');
INSERT INTO `cnvp_panel` VALUES ('20', null, '哈1', '哈1');
INSERT INTO `cnvp_panel` VALUES ('21', null, 'ss哈哈', 'ss哈哈');
INSERT INTO `cnvp_panel` VALUES ('22', null, 'adsasd', 'aa');
INSERT INTO `cnvp_panel` VALUES ('25', null, 'aab', 'b');
INSERT INTO `cnvp_panel` VALUES ('26', null, 'qw', '测试');
INSERT INTO `cnvp_panel` VALUES ('27', null, 'ååå1', 'ååå2');
INSERT INTO `cnvp_panel` VALUES ('28', null, 'æµè¯111aa', 'æµè¯111');
INSERT INTO `cnvp_panel` VALUES ('29', null, 'haha哈', '和蔼');
INSERT INTO `cnvp_panel` VALUES ('30', null, 'æµ', 'æµ');
INSERT INTO `cnvp_panel` VALUES ('31', '1', 'asd', 'asd');
INSERT INTO `cnvp_panel` VALUES ('32', '1', 'adsasd', 'asd');
INSERT INTO `cnvp_panel` VALUES ('33', '1', 'q', 'q');
INSERT INTO `cnvp_panel` VALUES ('34', '1', 'q', 'q');
INSERT INTO `cnvp_panel` VALUES ('35', '1', 'q', 'q');
INSERT INTO `cnvp_panel` VALUES ('36', '1', 'w', 'w');
INSERT INTO `cnvp_panel` VALUES ('37', '1', 'qw', 'qw');
INSERT INTO `cnvp_panel` VALUES ('38', '1', 'qw', 'qw');
INSERT INTO `cnvp_panel` VALUES ('39', '1', 'wq', 'qwe');
INSERT INTO `cnvp_panel` VALUES ('40', '1', 'ww and \'\'', 'ww and \'\'');
INSERT INTO `cnvp_panel` VALUES ('41', '1', '\' \'\'', ';--');
INSERT INTO `cnvp_panel` VALUES ('42', '1', 'qq', 'aa');
INSERT INTO `cnvp_panel` VALUES ('43', '1', '<script>alert(\"ss\")</script>', '1');

-- ----------------------------
-- Table structure for cnvp_phymapping
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_phymapping`;
CREATE TABLE `cnvp_phymapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `loc_id` int(11) NOT NULL,
  `phy_addr` varchar(512) DEFAULT NULL,
  `mac_addr` varchar(48) NOT NULL,
  `type` varchar(512) NOT NULL,
  `route` int(8) DEFAULT NULL,
  `tag` varchar(32) DEFAULT NULL,
  `describe` varchar(512) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_phymapping
-- ----------------------------
INSERT INTO `cnvp_phymapping` VALUES ('1', null, '3', '3', '1', '1', '1', '1', '1', '1');
INSERT INTO `cnvp_phymapping` VALUES ('2', null, '2', '2', '2', '2', '2', '2', '2', '2');

-- ----------------------------
-- Table structure for cnvp_project
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_project`;
CREATE TABLE `cnvp_project` (
  `desc` varchar(256) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(256) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of cnvp_project
-- ----------------------------
INSERT INTO `cnvp_project` VALUES ('111', null, '智能家居', '1');

-- ----------------------------
-- Table structure for cnvp_room
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_room`;
CREATE TABLE `cnvp_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(512) NOT NULL,
  `environment` varchar(512) DEFAULT NULL,
  `type` varchar(512) NOT NULL,
  `city` varchar(512) DEFAULT NULL,
  `building` varchar(512) DEFAULT NULL,
  `floor` int(8) DEFAULT NULL,
  `num_in_floor` varchar(32) DEFAULT NULL,
  `delete` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(255))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_room
-- ----------------------------
INSERT INTO `cnvp_room` VALUES ('1', null, '111', 'value3', 'value1', 'value2', 'value4', '1', '2', '0');
INSERT INTO `cnvp_room` VALUES ('2', null, 'value0', 'value3', 'value1', 'value2', 'value4', '1', '2', null);
INSERT INTO `cnvp_room` VALUES ('3', null, '测试', '测试', '测试', '测试', '测试', '12', '12', null);
INSERT INTO `cnvp_room` VALUES ('5', null, '测试', '测试', '测试', '测试', '测试', '23', '23', null);

-- ----------------------------
-- Table structure for cnvp_rule
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_rule`;
CREATE TABLE `cnvp_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `msgtype` varchar(128) DEFAULT NULL,
  `rule` varchar(1024) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `delete` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_rule
-- ----------------------------
INSERT INTO `cnvp_rule` VALUES ('3', '1', 'device1', 'select * from device1(name =\"deng\" and temp >40 and (1=1))', 'Mail', 'YES');
INSERT INTO `cnvp_rule` VALUES ('4', '1', 'device2', 'select * from device2(name =\"deng\" and rgb =\"red\" and (1=1))', 'SMS', '');
INSERT INTO `cnvp_rule` VALUES ('6', '1', 'device1', 'select * from device1(name =\"deng\" and temp >60 and (1=1))', 'Wechat', '');
INSERT INTO `cnvp_rule` VALUES ('7', '1', 'device1', 'select * from device1(name =\"deng\" and temp >50 and (1=1))', 'Mail', '');
INSERT INTO `cnvp_rule` VALUES ('8', '1', 'device1', 'select * from device1( user_id = 1 and logic_id = \"s\" and name = \"aaaa\" and (1=1))', null, null);
INSERT INTO `cnvp_rule` VALUES ('9', '1', 'device1', 'select * from device1( user_id = 1 and logic_id = \"s\" and name = \"s\" and (1=1))', null, null);

-- ----------------------------
-- Table structure for cnvp_rule1
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_rule1`;
CREATE TABLE `cnvp_rule1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `msgtype` varchar(128) DEFAULT NULL,
  `rule` varchar(1024) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `delete` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_rule1
-- ----------------------------
INSERT INTO `cnvp_rule1` VALUES ('3', '1', 'device1', 'select * from device1(name =\"deng\" and temp >40 and (1=1))', 'Mail', 'YES');
INSERT INTO `cnvp_rule1` VALUES ('4', '1', 'device2', 'select * from device2(name =\"deng\" and rgb =\"red\" and (1=1))', 'SMS', '');
INSERT INTO `cnvp_rule1` VALUES ('6', '1', 'device1', 'select * from device1(name =\"deng\" and temp >60 and (1=1))', 'Wechat', '');
INSERT INTO `cnvp_rule1` VALUES ('7', '1', 'device1', 'select * from device1(name =\"deng\" and temp >50 and (1=1))', 'Mail', '');

-- ----------------------------
-- Table structure for cnvp_scene
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_scene`;
CREATE TABLE `cnvp_scene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(512) NOT NULL,
  `image_id` varchar(512) DEFAULT NULL,
  `param1` varchar(512) DEFAULT NULL,
  `param2` varchar(512) DEFAULT NULL,
  `param3` varchar(512) DEFAULT NULL,
  `param4` varchar(512) DEFAULT NULL,
  `param5` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_scene
-- ----------------------------
INSERT INTO `cnvp_scene` VALUES ('1', '1', '柔和', 'rouhe_bg', '182', '255', '35', null, null);
INSERT INTO `cnvp_scene` VALUES ('2', '1', '舒适', 'shushi_bg', '151', '255', '78', null, null);
INSERT INTO `cnvp_scene` VALUES ('3', '1', '明亮', 'mingliang_bg', '72', '255', '105', null, null);
INSERT INTO `cnvp_scene` VALUES ('4', '1', '跳跃', 'tiaoyue_bg', '1', '2', '3', null, null);
INSERT INTO `cnvp_scene` VALUES ('5', '1', 'R', 'R_bg', '255', '0', '0', null, null);
INSERT INTO `cnvp_scene` VALUES ('6', '1', 'G', 'G_bg', '0', '255', '0', null, null);
INSERT INTO `cnvp_scene` VALUES ('7', '1', 'B', 'B_bg', '0', '0', '255', null, null);
INSERT INTO `cnvp_scene` VALUES ('8', '1', '观影', 'guanying', '0', '0', '255', null, null);
INSERT INTO `cnvp_scene` VALUES ('9', '1', '会客', 'huike', '1', '2', '3', null, null);
INSERT INTO `cnvp_scene` VALUES ('10', '1', '浪漫', 'langman', '100', '200', 's0', null, null);
INSERT INTO `cnvp_scene` VALUES ('11', '1', '睡眠', 'shuimian', '50', '0', '200', null, null);
INSERT INTO `cnvp_scene` VALUES ('12', '1', '晚餐', 'wancan', '198', '200', '1', null, null);
INSERT INTO `cnvp_scene` VALUES ('13', '1', '阅读', 'yuedu', '111', '222', '100', null, null);

-- ----------------------------
-- Table structure for cnvp_sceneconfig
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sceneconfig`;
CREATE TABLE `cnvp_sceneconfig` (
  `id` int(50) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `scene_id` int(11) DEFAULT NULL,
  `equipment_id` int(11) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `param1` varchar(512) DEFAULT NULL,
  `param2` varchar(512) DEFAULT NULL,
  `param3` varchar(512) DEFAULT NULL,
  `param4` varchar(512) DEFAULT NULL,
  `param5` varchar(512) DEFAULT NULL,
  `scene_name` varchar(512) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `equipment_logic_id` varchar(512) DEFAULT NULL,
  `room_name` varchar(512) DEFAULT NULL,
  `tag` varchar(512) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  KEY `sceneFK` (`scene_id`),
  KEY `equipmentFK` (`equipment_id`),
  CONSTRAINT `equipmentFK` FOREIGN KEY (`equipment_id`) REFERENCES `cnvp_equipment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_sceneconfig
-- ----------------------------
INSERT INTO `cnvp_sceneconfig` VALUES ('1', null, '1', '1', 'value1', 'value2', 'value1', 'value2', 'value1', 'value2', null, null, null, null, null, null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1', '1', '1', '42', '0', '0', '0', '0', '0', '0', null, null, null, null, null, null);
INSERT INTO `cnvp_sceneconfig` VALUES ('2', '1', '2', '42', '0', '0', '0', '0', '0', '0', null, null, null, null, null, null);
INSERT INTO `cnvp_sceneconfig` VALUES ('3', '1', '3', '42', '0', '0', '0', '0', '0', '0', null, null, null, null, null, null);
INSERT INTO `cnvp_sceneconfig` VALUES ('450', '1', '1', '45', '0', '0', '0', '0', '0', '0', null, null, null, null, null, null);
INSERT INTO `cnvp_sceneconfig` VALUES ('451', '1', '2', '45', '0', '0', '0', '0', '0', '0', null, null, null, null, null, null);
INSERT INTO `cnvp_sceneconfig` VALUES ('452', '1', '3', '45', '0', '0', '0', '0', '0', '0', null, null, null, null, null, null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1280', '75', '1', '128', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '166', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1281', '75', '2', '128', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '166', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1282', '75', '3', '128', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '166', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1283', '75', '4', '128', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '166', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1284', '75', '5', '128', '0', '255', '0', '0', null, null, 'R', 'R_bg', '166', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1285', '75', '6', '128', '0', '0', '255', '0', null, null, 'G', 'G_bg', '166', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1286', '75', '7', '128', '0', '0', '0', '255', null, null, 'B', 'B_bg', '166', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1287', '75', '8', '128', '0', '0', '0', '255', null, null, '观影', 'guanying', '166', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1288', '75', '9', '128', '0', '1', '2', '3', null, null, '会客', 'huike', '166', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1289', '75', '10', '128', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '166', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1290', '75', '11', '128', '0', '50', '0', '200', null, null, '睡眠', 'shuimian', '166', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1291', '75', '12', '128', '0', '198', '200', '1', null, null, '晚餐', 'wancan', '166', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1292', '75', '13', '128', '0', '111', '222', '100', null, null, '阅读', 'yuedu', '166', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1290', '74', '1', '129', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1291', '74', '2', '129', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1292', '74', '3', '129', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1293', '74', '4', '129', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1294', '74', '5', '129', '0', '255', '0', '0', null, null, 'R', 'R_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1295', '74', '6', '129', '0', '0', '255', '0', null, null, 'G', 'G_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1296', '74', '7', '129', '0', '0', '0', '255', null, null, 'B', 'B_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1297', '74', '8', '129', '0', '0', '0', '255', null, null, '观影', 'guanying', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1298', '74', '9', '129', '0', '1', '2', '3', null, null, '会客', 'huike', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1299', '74', '10', '129', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1300', '74', '11', '129', '0', '50', '0', '200', null, null, '睡眠', 'shuimian', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1301', '74', '12', '129', '0', '198', '200', '1', null, null, '晚餐', 'wancan', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1302', '74', '13', '129', '0', '111', '222', '100', null, null, '阅读', 'yuedu', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('103352353', '74', '-1', '129', null, '255', '255', '255', null, null, '111', '13262FC2-4337-48D3-8626-9E799ACB0786', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1310', '73', '1', '131', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1311', '73', '2', '131', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1312', '73', '3', '131', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1313', '73', '4', '131', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1314', '73', '5', '131', '0', '255', '0', '0', null, null, 'R', 'R_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1315', '73', '6', '131', '0', '0', '255', '0', null, null, 'G', 'G_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1316', '73', '7', '131', '0', '0', '0', '255', null, null, 'B', 'B_bg', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1317', '73', '8', '131', '0', '0', '0', '255', null, null, '观影', 'guanying', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1318', '73', '9', '131', '0', '1', '2', '3', null, null, '会客', 'huike', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1319', '73', '10', '131', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1320', '73', '11', '131', '0', '50', '0', '200', null, null, '睡眠', 'shuimian', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1321', '73', '12', '131', '0', '198', '200', '1', null, null, '晚餐', 'wancan', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1322', '73', '13', '131', '0', '111', '222', '100', null, null, '阅读', 'yuedu', '143', '-1', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('90516724', '73', '-1', '131', null, '255', '255', '255', null, null, '123', '9AA3A472-30BB-4EB8-A26D-451FECA20092', '143', '-1', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1350', '73', '1', '135', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '144', '客厅', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1351', '73', '2', '135', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '144', '客厅', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1352', '73', '3', '135', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '144', '客厅', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1353', '73', '4', '135', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '144', '客厅', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1354', '73', '5', '135', '0', '255', '0', '0', null, null, 'R', 'R_bg', '144', '客厅', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1355', '73', '6', '135', '0', '0', '255', '0', null, null, 'G', 'G_bg', '144', '客厅', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1356', '73', '7', '135', '0', '0', '0', '255', null, null, 'B', 'B_bg', '144', '客厅', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1357', '73', '8', '135', '0', '0', '0', '255', null, null, '观影', 'guanying', '144', '客厅', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1358', '73', '9', '135', '0', '1', '2', '3', null, null, '会客', 'huike', '144', '客厅', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1359', '73', '10', '135', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '144', '客厅', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1360', '73', '11', '135', '0', '50', '0', '200', null, null, '睡眠', 'shuimian', '144', '客厅', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1361', '73', '12', '135', '0', '198', '200', '1', null, null, '晚餐', 'wancan', '144', '客厅', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1362', '73', '13', '135', '0', '111', '222', '100', null, null, '阅读', 'yuedu', '144', '客厅', '1', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('94126939', '73', '-1', '135', null, '96', '0', '0', null, null, '123', '8852BA0E-6C75-4352-97D6-813BA6C3B8B0', '144', '客厅', '0', null);
INSERT INTO `cnvp_sceneconfig` VALUES ('1430', '72', '1', '143', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '168', '客厅', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1431', '72', '2', '143', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '168', '客厅', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1432', '72', '3', '143', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '168', '客厅', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1433', '72', '4', '143', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '168', '客厅', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1434', '72', '5', '143', '0', '255', '0', '0', null, null, 'R', 'R_bg', '168', '客厅', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1435', '72', '6', '143', '0', '0', '255', '0', null, null, 'G', 'G_bg', '168', '客厅', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1436', '72', '7', '143', '0', '0', '0', '255', null, null, 'B', 'B_bg', '168', '客厅', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1437', '72', '8', '143', '0', '0', '0', '255', null, null, '观影', 'guanying', '168', '客厅', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1438', '72', '9', '143', '0', '1', '2', '3', null, null, '会客', 'huike', '168', '客厅', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1439', '72', '10', '143', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '168', '客厅', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1440', '72', '11', '143', '0', '50', '0', '200', null, null, '睡眠', 'shuimian', '168', '客厅', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1441', '72', '12', '143', '0', '198', '200', '1', null, null, '晚餐', 'wancan', '168', '客厅', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1442', '72', '13', '143', '0', '111', '222', '100', null, null, '阅读', 'yuedu', '168', '客厅', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1440', '72', '1', '144', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '166', '客厅', '0', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1441', '72', '2', '144', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '166', '客厅', '0', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1442', '72', '3', '144', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '166', '客厅', '0', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1443', '72', '4', '144', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '166', '客厅', '0', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1444', '72', '5', '144', '0', '255', '0', '0', null, null, 'R', 'R_bg', '166', '客厅', '0', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1445', '72', '6', '144', '0', '0', '255', '0', null, null, 'G', 'G_bg', '166', '客厅', '0', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1446', '72', '7', '144', '0', '0', '0', '255', null, null, 'B', 'B_bg', '166', '客厅', '0', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1447', '72', '8', '144', '0', '0', '0', '255', null, null, '观影', 'guanying', '166', '客厅', '1', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1448', '72', '9', '144', '0', '1', '2', '3', null, null, '会客', 'huike', '166', '客厅', '1', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1449', '72', '10', '144', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '166', '客厅', '1', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('1450', '72', '1', '145', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1451', '72', '2', '145', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1452', '72', '3', '145', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1450', '72', '1', '145', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1451', '72', '2', '145', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1452', '72', '3', '145', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1453', '72', '4', '145', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1454', '72', '5', '145', '0', '255', '0', '0', null, null, 'R', 'R_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1455', '72', '6', '145', '0', '0', '255', '0', null, null, 'G', 'G_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1456', '72', '7', '145', '0', '0', '0', '255', null, null, 'B', 'B_bg', '143', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1457', '72', '8', '145', '0', '0', '0', '255', null, null, '观影', 'guanying', '143', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1458', '72', '9', '145', '0', '1', '2', '3', null, null, '会客', 'huike', '143', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1459', '72', '10', '145', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '143', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1460', '75', '1', '146', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1461', '75', '2', '146', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1462', '75', '3', '146', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1460', '75', '1', '146', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1461', '75', '2', '146', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1462', '75', '3', '146', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1463', '75', '4', '146', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1464', '75', '5', '146', '0', '255', '0', '0', null, null, 'R', 'R_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1465', '75', '6', '146', '0', '0', '255', '0', null, null, 'G', 'G_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1466', '75', '7', '146', '0', '0', '0', '255', null, null, 'B', 'B_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1467', '75', '8', '146', '0', '0', '0', '255', null, null, '观影', 'guanying', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1468', '75', '9', '146', '0', '1', '2', '3', null, null, '会客', 'huike', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1469', '75', '10', '146', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1470', '75', '11', '146', '0', '50', '0', '200', null, null, '睡眠', 'shuimian', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1471', '75', '12', '146', '0', '198', '200', '1', null, null, '晚餐', 'wancan', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1472', '75', '13', '146', '0', '111', '222', '100', null, null, '阅读', 'yuedu', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1470', '82', '1', '147', '0', '182', '255', '35', null, null, '柔和', 'rouhe_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1471', '82', '2', '147', '0', '151', '255', '78', null, null, '舒适', 'shushi_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1472', '82', '3', '147', '0', '72', '255', '105', null, null, '明亮', 'mingliang_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1473', '82', '4', '147', '0', '1', '2', '3', null, null, '跳跃', 'tiaoyue_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1474', '82', '5', '147', '0', '255', '0', '0', null, null, 'R', 'R_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1475', '82', '6', '147', '0', '0', '255', '0', null, null, 'G', 'G_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1476', '82', '7', '147', '0', '0', '0', '255', null, null, 'B', 'B_bg', '165', '-1', '0', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1477', '82', '8', '147', '0', '0', '0', '255', null, null, '观影', 'guanying', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1478', '82', '9', '147', '0', '1', '2', '3', null, null, '会客', 'huike', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1479', '82', '10', '147', '0', '100', '200', 's0', null, null, '浪漫', 'langman', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1480', '82', '11', '147', '0', '50', '0', '200', null, null, '睡眠', 'shuimian', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1481', '82', '12', '147', '0', '198', '200', '1', null, null, '晚餐', 'wancan', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('1482', '82', '13', '147', '0', '111', '222', '100', null, null, '阅读', 'yuedu', '165', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('55653262', '72', '-1', '143', null, '55', '55', '55', null, null, 'gu123', 'guanying', '168', '客厅', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('55653265', '72', '-1', '145', null, '36', '0', '255', null, null, 'gu1', 'guanying', '143', '-1', '1', '40');
INSERT INTO `cnvp_sceneconfig` VALUES ('55653267', '72', '-1', '144', null, '78', '0', '0', null, null, 'gu1', 'guanying', '166', '客厅', '1', '41');
INSERT INTO `cnvp_sceneconfig` VALUES ('71239291', '72', '-1', '145', null, '255', '255', '255', null, null, 'trf', 'F09C7AF3-1F22-4430-A393-87E01477E84A', '143', '-1', '0', null);

-- ----------------------------
-- Table structure for cnvp_simulator_msg
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_simulator_msg`;
CREATE TABLE `cnvp_simulator_msg` (
  `devicetype` varchar(256) DEFAULT NULL,
  `frequency` varchar(11) DEFAULT NULL,
  `message` varchar(256) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of cnvp_simulator_msg
-- ----------------------------
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '122', '{\"logic_id\":22,\"id\":\"2\",\"name\":\"112\",\"temp\":\"awwq\"}', '4');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '223', '{\"logic_id\":2332,\"id\":\"23\",\"name\":\"2dss\",\"temp\":\"we\"}', '5');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '23', '{\"logic_id\":23,\"id\":\"233\",\"name\":\"sedqew\",\"temp\":\"qewq\"}', '6');
INSERT INTO `cnvp_simulator_msg` VALUES ('device2', '12', '{\"logic_id\":123,\"id\":\"123\",\"name\":\"23\",\"light\":\"weq\",\"temp\":\"12\",\"rgb\":\"asdeqe\"}', '7');
INSERT INTO `cnvp_simulator_msg` VALUES ('device2', '123', '{\"logic_id\":123,\"id\":\"21\",\"name\":\"qaeq\",\"light\":\"123\",\"temp\":\"23\",\"rgb\":\"123\"}', '8');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '23', '{\"logic_id\":123,\"id\":\"23\",\"name\":\"qwe\",\"temp\":\"13\"}', '9');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '23', '{\"logic_id\":23,\"id\":\"23\",\"name\":\"23\",\"temp\":\"23\"}', '10');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '12', '{\"logic_id\":11,\"id\":\"1\",\"name\":\"2\",\"temp\":\"23\"}', '11');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"logic_id\":a,\"id\":\"2\",\"name\":\"2\",\"temp\":\"2\"}', '12');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '13');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '14');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '15');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '13', '{\"logic_id\":1,\"id\":\"3213\",\"name\":\"tewst\",\"temp\":\"fda\"}', '16');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"qer\",\"name\":\"fdas\",\"temp\":\"rfedg\"}', '17');
INSERT INTO `cnvp_simulator_msg` VALUES ('device2', '7', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"light\":\"4\",\"temp\":\"5\",\"rgb\":\"6\"}', '18');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '19');
INSERT INTO `cnvp_simulator_msg` VALUES ('device2', '1', '{\"logic_id\":1,\"id\":\"1\",\"name\":\"1\",\"light\":\"1\",\"temp\":\"1\",\"rgb\":\"1\"}', '20');
INSERT INTO `cnvp_simulator_msg` VALUES ('bumb', '2', '{\"logic_id\":2,\"id\":\"2\",\"price\":\"2\",\"name\":\"2\"}', '21');
INSERT INTO `cnvp_simulator_msg` VALUES ('test111', '1', '{\"logic_id\":1,\"id\":\"1\",\"price\":\"1\",\"name\":\"1\"}', '22');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"logic_id\":2,\"id\":\"2\",\"name\":\"2\",\"temp\":\"2\"}', '23');
INSERT INTO `cnvp_simulator_msg` VALUES ('test', '3', '{\"logic_id\":3,\"id\":\"3\",\"price\":\"3\",\"name\":\"3\"}', '24');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"1\",\"name\":\"11\",\"temp\":\"1\"}', '25');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '26');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '6', '{\"logic_id\":1,\"id\":\"3\",\"name\":\"4\",\"temp\":\"5\"}', '27');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"logic_id\":2,\"id\":\"2\",\"name\":\"2\",\"temp\":\"2\"}', '28');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '3', '{\"logic_id\":3,\"id\":\"3\",\"name\":\"3\",\"temp\":\"3\"}', '29');
INSERT INTO `cnvp_simulator_msg` VALUES ('device2', '5', '{\"logic_id\":5,\"id\":\"5\",\"name\":\"5\",\"light\":\"5\",\"temp\":\"5\",\"rgb\":\"5\"}', '30');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '1', '{\"logic_id\":1,\"id\":\"1\",\"name\":\"1\",\"temp\":\"1\"}', '31');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '4', '{\"logic_id\":,\"id\":\"123213\",\"name\":\"4\",\"temp\":\"4\"}', '32');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '33');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '1', '{\"logic_id\":1,\"id\":\"1\",\"name\":\"1\",\"temp\":\"1\"}', '34');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '1', '{\"logic_id\":1,\"id\":\"1\",\"name\":\"1\",\"temp\":\"1\"}', '35');
INSERT INTO `cnvp_simulator_msg` VALUES ('device2', '4', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"light\":\"4\",\"temp\":\"4\",\"rgb\":\"4\"}', '36');
INSERT INTO `cnvp_simulator_msg` VALUES ('device2', '2', '{\"logic_id\":1,\"id\":\"1\",\"name\":\"1\",\"light\":\"1\",\"temp\":\"1\",\"rgb\":\"1\"}', '37');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '5', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '38');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '1', '{\"logic_id\":1,\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '39');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '1', '{\"id\":\"2\",\"name\":\"3\",\"temp\":\"4\"}', '40');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"id\":\"2\",\"name\":\"2\",\"temp\":\"23\"}', '41');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"id\":\"2\",\"name\":\"2\",\"temp\":\"3\"}', '42');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"id\":\"12\",\"name\":\"22\",\"temp\":\"23\"}', '43');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"id\":\"213\",\"name\":\"12\",\"temp\":\"12\"}', '44');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"id\":\"213\",\"name\":\"deng\",\"temp\":\"267\"}', '45');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"id\":213,\"name\":\"deng\",\"temp\":235}', '46');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"id\":\"213\",\"name\":\"deng\",\"temp\":234}', '47');
INSERT INTO `cnvp_simulator_msg` VALUES ('device1', '2', '{\"id\":\"213\",\"name\":\"deng\",\"temp\":232}', '48');

-- ----------------------------
-- Table structure for cnvp_sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_dept`;
CREATE TABLE `cnvp_sys_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `cname` varchar(200) NOT NULL COMMENT '名称',
  `desc` varchar(500) DEFAULT NULL COMMENT '描述',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级Id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_sys_dept
-- ----------------------------
INSERT INTO `cnvp_sys_dept` VALUES ('1', '杭州捷点', null, '0');
INSERT INTO `cnvp_sys_dept` VALUES ('16', '电商', null, '1');
INSERT INTO `cnvp_sys_dept` VALUES ('14', '技术部', null, '15');
INSERT INTO `cnvp_sys_dept` VALUES ('15', '科技', null, '1');
INSERT INTO `cnvp_sys_dept` VALUES ('8', '客服部', null, '15');
INSERT INTO `cnvp_sys_dept` VALUES ('9', '商务部', null, '15');
INSERT INTO `cnvp_sys_dept` VALUES ('17', 'A部门', null, '16');
INSERT INTO `cnvp_sys_dept` VALUES ('18', 'B部门', null, '16');

-- ----------------------------
-- Table structure for cnvp_sys_nav
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_nav`;
CREATE TABLE `cnvp_sys_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统id',
  `title` varchar(100) NOT NULL COMMENT '文字',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `url` varchar(100) DEFAULT NULL COMMENT '链接',
  `target` varchar(20) NOT NULL DEFAULT 'mainFrame' COMMENT '目标',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `orderid` int(11) NOT NULL DEFAULT '10' COMMENT '排序',
  `res_id` int(11) NOT NULL DEFAULT '0' COMMENT '资源Id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='导航';

-- ----------------------------
-- Records of cnvp_sys_nav
-- ----------------------------
INSERT INTO `cnvp_sys_nav` VALUES ('1', '个人资料', 'icon-home', null, 'mainFrame', '0', '4', '88');
INSERT INTO `cnvp_sys_nav` VALUES ('3', '系统', 'icon-cog', null, 'mainFrame', '0', '2', '89');
INSERT INTO `cnvp_sys_nav` VALUES ('33', '组织结构', 'icon-sitemap', null, 'mainFrame', '3', '2', '48');
INSERT INTO `cnvp_sys_nav` VALUES ('69', '项目列表', 'icon-music', null, 'mainFrame', '68', '10', '170');
INSERT INTO `cnvp_sys_nav` VALUES ('65', '修改头像', 'icon-qrcode', null, 'mainFrame', '1', '5', '167');
INSERT INTO `cnvp_sys_nav` VALUES ('18', '个人资料', 'icon-user', null, 'mainFrame', '1', '2', '79');
INSERT INTO `cnvp_sys_nav` VALUES ('19', '修改密码', 'icon-key', null, 'mainFrame', '1', '3', '80');
INSERT INTO `cnvp_sys_nav` VALUES ('2', '开发', 'icon-codepen', null, 'mainFrame', '0', '5', '90');
INSERT INTO `cnvp_sys_nav` VALUES ('15', '12312312', '312', null, 'mainFrame', '123', '10', '0');
INSERT INTO `cnvp_sys_nav` VALUES ('68', '项目管理', 'icon-glass', null, 'mainFrame', '0', '6', '169');
INSERT INTO `cnvp_sys_nav` VALUES ('67', '模拟器', 'icon-list', null, 'mainFrame', '2', '10', '168');
INSERT INTO `cnvp_sys_nav` VALUES ('23', '系统设置', 'icon-desktop', null, 'mainFrame', '3', '1', '3');
INSERT INTO `cnvp_sys_nav` VALUES ('24', '导航菜单', 'icon-th-list', null, 'mainFrame', '3', '6', '73');
INSERT INTO `cnvp_sys_nav` VALUES ('26', '文件管理', 'icon-file', null, 'mainFrame', '3', '7', '0');
INSERT INTO `cnvp_sys_nav` VALUES ('27', '日志查询', 'icon-angle-double-right', null, 'mainFrame', '3', '9', '0');
INSERT INTO `cnvp_sys_nav` VALUES ('32', '用户管理', 'icon-user', null, 'mainFrame', '3', '3', '54');
INSERT INTO `cnvp_sys_nav` VALUES ('34', '角色管理', 'icon-group', null, 'mainFrame', '3', '4', '60');
INSERT INTO `cnvp_sys_nav` VALUES ('35', '资源管理', 'icon-unlock-alt', null, 'mainFrame', '3', '5', '67');
INSERT INTO `cnvp_sys_nav` VALUES ('66', '欢迎使用', 'icon-gear', null, 'mainFrame', '1', '1', '78');
INSERT INTO `cnvp_sys_nav` VALUES ('37', '云平台', 'icon-send', null, 'mainFrame', '0', '1', '105');
INSERT INTO `cnvp_sys_nav` VALUES ('38', '设备管理', 'icon-cubes', null, 'mainFrame', '37', '2', '100');
INSERT INTO `cnvp_sys_nav` VALUES ('39', '房间管理', 'icon-th', null, 'mainFrame', '37', '1', '106');
INSERT INTO `cnvp_sys_nav` VALUES ('40', '物理设备管理', 'icon-gears', null, 'mainFrame', '37', '3', '111');
INSERT INTO `cnvp_sys_nav` VALUES ('41', '面板管理', 'icon-credit-card', null, 'mainFrame', '37', '4', '116');
INSERT INTO `cnvp_sys_nav` VALUES ('42', '网关管理', 'icon-retweet', null, 'mainFrame', '37', '5', '120');
INSERT INTO `cnvp_sys_nav` VALUES ('43', '情景管理', 'icon-eye', null, 'mainFrame', '37', '6', '125');
INSERT INTO `cnvp_sys_nav` VALUES ('44', '情景配置', 'icon-bar-chart-o', null, 'mainFrame', '37', '7', '130');
INSERT INTO `cnvp_sys_nav` VALUES ('60', '添加消息模板', 'icon-music', null, 'mainFrame', '59', '10', '153');
INSERT INTO `cnvp_sys_nav` VALUES ('64', '接口测试', 'icon-th', null, 'mainFrame', '2', '10', '162');
INSERT INTO `cnvp_sys_nav` VALUES ('62', '修改邮箱', 'icon-th-list', null, 'mainFrame', '1', '4', '144');
INSERT INTO `cnvp_sys_nav` VALUES ('63', '添加规则', 'icon-star-o', null, 'mainFrame', '59', '10', '141');
INSERT INTO `cnvp_sys_nav` VALUES ('59', '规则管理', 'icon-glass', null, 'mainFrame', '0', '3', '88');
INSERT INTO `cnvp_sys_nav` VALUES ('74', '产品管理', 'icon-music', null, 'mainFrame', '68', '10', '153');
INSERT INTO `cnvp_sys_nav` VALUES ('71', '设备列表', 'icon-envelope-o', null, 'mainFrame', '70', '10', '100');
INSERT INTO `cnvp_sys_nav` VALUES ('75', '设备管理', 'icon-search', null, 'mainFrame', '68', '10', '100');
INSERT INTO `cnvp_sys_nav` VALUES ('73', '产品列表', 'icon-search', null, 'mainFrame', '72', '10', '153');
INSERT INTO `cnvp_sys_nav` VALUES ('77', '统计', 'icon-music', null, 'mainFrame', '0', '10', '173');
INSERT INTO `cnvp_sys_nav` VALUES ('78', '设备控制', 'icon-gear', null, 'mainFrame', '37', '10', '179');

-- ----------------------------
-- Table structure for cnvp_sys_res
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_res`;
CREATE TABLE `cnvp_sys_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统Id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级Id',
  `cname` varchar(50) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '权限识别码',
  `code_route` varchar(500) DEFAULT NULL COMMENT '权限路径',
  `des` varchar(200) DEFAULT NULL COMMENT '描述',
  `ak` varchar(200) DEFAULT NULL COMMENT 'actionKey',
  `seq` int(11) DEFAULT '10' COMMENT '排序',
  `type` tinyint(2) DEFAULT NULL COMMENT '1、actionKey；',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_sys_res
-- ----------------------------
INSERT INTO `cnvp_sys_res` VALUES ('1', '0', '个人资料', 'start', 'start', '开始', null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('2', '0', '系统', 'system', 'system', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('3', '2', '系统设置', 'config', 'system:config', null, '/System', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('47', '2', '组织机构', 'dept', 'system:dept', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('48', '47', '首页', 'index', 'system:dept:index', null, '/Dept', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('49', '47', '抽取json数据', 'getlist', 'system:dept:getlist', null, '/Dept/getlist', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('50', '47', '创建', 'create', 'system:dept:create', null, '/Dept/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('51', '47', '更新', 'update', 'system:dept:update', null, '/Dept/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('52', '47', '删除', 'delete', 'system:dept:delete', null, '/Dept/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('53', '2', '用户管理', 'user', 'system:user', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('54', '53', '列表', 'index', 'system:user:index', null, '/User', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('55', '53', '添加', 'create', 'system:user:create', null, '/User/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('56', '53', '修改', 'update', 'system:user:update', null, '/User/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('57', '53', '删除', 'delete', 'system:user:delete', null, '/User/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('58', '53', '批量删除', 'deleteall', 'system:user:deleteall', null, '/User/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('59', '2', '角色管理', 'role', 'system:role', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('60', '59', '列表', 'index', 'system:role:index', null, '/Role', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('61', '59', '添加', 'create', 'system:role:create', null, '/Role/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('62', '59', '更新', 'update', 'system:role:update', null, '/Role/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('63', '59', '删除', 'delete', 'system:role:delete', null, '/Role/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('64', '59', '批量删除', 'deleteAll', 'system:role:deleteAll', null, '/Role/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('65', '59', '抽取JSON数据', 'getlist', 'system:role:getlist', null, '/Role/getlist', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('66', '2', '资源管理', 'res', 'system:res', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('67', '66', '首页', 'index', 'system:res:index', null, '/Resource', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('68', '66', '添加', 'create', 'system:res:create', null, '/Role/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('69', '66', '更新', 'update', 'system:res:update', null, '/Resource/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('70', '66', '删除', 'delete', 'system:res:delete', null, '/Resource/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('71', '66', '抽取JSON数据', 'getlist', 'system:res:getlist', null, '/Resource/getlist', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('72', '2', '导航管理', 'sysnav', 'system:sysnav', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('73', '72', '首页', 'index', 'system:sysnav:index', null, '/System/nav', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('74', '72', '添加', 'create', 'system:sysnav:create', null, '/System/nav_create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('75', '72', '更新', 'update', 'system:sysnav:update', null, '/System/nav_update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('76', '72', '删除', 'delete', 'system:sysnav:delete', null, '/System/nav_delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('77', '72', '保存排序', 'saveorder', 'system:sysnav:saveorder', null, '/System/save_order', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('78', '1', '欢迎使用', 'welcome', 'start:welcome', null, '/welcome', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('79', '1', '个人资料', 'profile', 'start:profile', null, '/profile', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('80', '1', '修改密码', 'password', 'start:password', null, '/User/password', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('81', '0', '开发', 'dev', 'dev', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('82', '81', '首页', 'index', 'dev:index', null, '/Generator', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('83', '81', '模型代码', 'model', 'dev:model', null, '/Generator/model_code', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('84', '81', '控制器代码', 'controller', 'dev:controller', null, '/Generator/controller_code', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('85', '81', '视图代码', 'view', 'dev:view', null, '/Generator/view_code', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('86', '59', '配置资源权限', 'set_res', 'system:role:set_res', null, '/Role/set_res', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('146', '0', '管理首页', 'frame', 'frame', null, '/', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('88', '1', '头部导航', 'topnav', 'start:topnav', null, null, '0', '0');
INSERT INTO `cnvp_sys_res` VALUES ('89', '2', '头部导航', 'topnav', 'system:topnav', null, null, '0', '0');
INSERT INTO `cnvp_sys_res` VALUES ('90', '81', '头部导航', 'topnav', 'dev:topnav', null, null, '0', '0');
INSERT INTO `cnvp_sys_res` VALUES ('91', '0', '智能', 'intelligence', 'intelligence', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('94', '91', '设备', 'equipment', 'intelligence:equipment', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('93', '91', '房间', 'room', 'intelligence:room', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('95', '91', '物理设备', 'phymapping', 'intelligence:phymapping', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('96', '91', '面板', 'panel', 'intelligence:panel', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('97', '91', '网关', 'netcontroller', 'intelligence:netcontroller', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('98', '91', '情景', 'scene', 'intelligence:scene', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('99', '91', '情景配置', 'sceneconfig', 'intelligence:sceneconfig', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('100', '94', '列表', 'index', 'intelligence:equipment:index', null, '/Equipment', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('101', '94', '添加', 'create', 'intelligence:equipment:create', null, '/Equipment/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('102', '94', '修改', 'update', 'intelligence:equipment:update', null, '/Equipment/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('103', '94', '删除', 'delete', 'intelligence:equipment:delete', null, '/Equipment/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('104', '94', '批量删除', 'deleteall', 'intelligence:equipment:deleteall', null, '/Equipment/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('105', '91', '头部导航', 'topnav', 'intelligence:topnav', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('106', '93', '列表', 'index', 'intelligence:room:index', null, '/Room', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('107', '93', '添加', 'create', 'intelligence:room:create', null, '/Room/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('108', '93', '修改', 'update', 'intelligence:room:update', null, '/Room/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('109', '93', '删除', 'delete', 'intelligence:room:delete', null, '/Room/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('110', '93', '批量删除', 'deleteall', 'intelligence:room:deleteall', null, '/Room/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('111', '95', '列表', 'index', 'intelligence:phymapping:index', null, '/Phymapping', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('112', '95', '添加', 'create', 'intelligence:phymapping:create', null, '/Phymapping/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('113', '95', '修改', 'update', 'intelligence:phymapping:update', null, '/Phymapping/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('114', '95', '删除', 'delete', 'intelligence:phymapping:delete', null, '/Phymapping/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('115', '95', '批量删除', 'deleteall', 'intelligence:phymapping:deleteall', null, '/Phymapping/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('116', '96', '列表', 'index', 'intelligence:panel:index', null, '/Panel', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('117', '96', '添加', 'create', 'intelligence:panel:create', null, '/Panel/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('118', '96', '删除', 'delete', 'intelligence:panel:delete', null, '/Panel/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('119', '96', '删除所有', 'deleteall', 'intelligence:panel:deleteall', null, '/Panel/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('120', '97', '列表', 'index', 'intelligence:netcontroller:index', null, '/Netctl', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('121', '97', '添加', 'create', 'intelligence:netcontroller:create', null, '/Netctl/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('122', '97', '删除', 'delete', 'intelligence:netcontroller:delete', null, '/Netctl/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('123', '97', '修改', 'update', 'intelligence:netcontroller:update', null, '/Netctl/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('124', '96', '修改', 'update', 'intelligence:panel:update', null, '/Panel/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('125', '98', '列表', 'index', 'intelligence:scene:index', null, '/Scene', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('126', '98', '添加', 'create', 'intelligence:scene:create', null, '/Scene/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('127', '98', '修改', 'update', 'intelligence:scene:update', null, '/Scene/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('128', '98', '删除', 'delete', 'intelligence:scene:delete', null, '/Scene/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('129', '98', '批量删除', 'deleteall', 'intelligence:scene:deleteall', null, '/Scene/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('130', '99', '列表', 'index', 'intelligence:sceneconfig:index', null, '/Sceneconfig', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('131', '99', '添加', 'create', 'intelligence:sceneconfig:create', null, '/Sceneconfig/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('132', '99', '修改', 'update', 'intelligence:sceneconfig:update', null, '/Sceneconfig/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('133', '99', '删除', 'delete', 'intelligence:sceneconfig:delete', null, '/Sceneconfig/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('134', '99', '批量删除', 'deleteall', 'intelligence:sceneconfig:deleteall', null, '/Sceneconfig/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('135', '0', '规则', 'rule', 'rule', '测试', null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('148', '135', '添加', 'create', 'rule:create', null, '/Rule/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('141', '135', '列表', 'index', 'rule:index', null, '/Rule', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('144', '1', '修改邮箱', 'email', 'start:email', null, '/User/email', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('149', '135', '删除', 'delete', 'rule:delete', null, '/Rule/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('150', '135', '删除所有', 'deletaAll', 'rule:deletaAll', null, '/Rule/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('151', '135', '修改', 'update', 'rule:update', null, '/Rule/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('152', '0', '产品管理', 'messagetype', 'messagetype', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('153', '152', '列表', 'index', 'messagetype:index', null, '/MessageType', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('154', '152', '添加', 'create', 'messagetype:create', null, '/MessageType/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('155', '152', '删除', 'delete', 'messagetype:delete', null, '/MessageType/delete', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('156', '152', '删除所有', 'deleteAll', 'messagetype:deleteAll', null, '/MessageType/deleteAll', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('157', '152', '修改', 'update', 'messagetype:update', null, '/MessageType/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('159', '135', '设备信息', 'equipment', 'rule:equipment', null, '/Rule/getEquipment', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('160', '135', '更新', 'update', 'rule:update', null, '/Rule/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('161', '135', '更新', 'update', 'rule:update', null, '/Rule/update', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('162', '0', '接口测试', 'test', 'test', null, '/test', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('163', '94', '查找', 'find', 'intelligence:equipment:find', null, '/Equipment/find', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('165', '94', '获取房间', 'getRoom', 'intelligence:equipment:getRoom', null, '/Equipment/getRoom', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('166', '94', '获取面板', 'getPanel', 'intelligence:equipment:getPanel', null, '/Equipment/getPanel', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('167', '1', '修改头像', 'portrait', 'start:portrait', null, '/portrait', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('168', '81', '模拟器', 'simulator', 'dev:simulator', null, '/Simulator/getDeviceType', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('169', '0', '项目管理', 'createproject', 'createproject', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('170', '169', '项目列表', 'index', 'createproject:index', null, '/Project', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('171', '169', '添加', 'create', 'createproject:create', null, '/Project/create', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('173', '0', '统计', ' statistic', ' statistic', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('175', '93', '查找', 'find', 'intelligence:room:find', '查找', '/Room/find', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('176', '98', '查找', 'find', 'intelligence:scene:find', '查找', '/Scene/find', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('177', '99', '查找', 'find', 'intelligence:sceneconfig:find', '查找', '/Sceneconfig/find', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('178', '91', '设备控制', 'DeviceControl', 'intelligence:DeviceControl', null, null, '10', '0');
INSERT INTO `cnvp_sys_res` VALUES ('179', '178', '设备列表', 'index', 'intelligence:DeviceControl:index', null, '/Device', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('180', '178', '设备状态', 'getStatus', 'intelligence:DeviceControl:getStatus', null, '/Device/getStatus', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('181', '178', '设备控制', 'control', 'intelligence:DeviceControl:control', null, '/Device/control', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('185', '99', '下载图片', 'portraitdownload', 'intelligence:sceneconfig:portraitdownload', '下载', '/Sceneconfig/portrait_download', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('184', '99', '上传图片', 'portrait', 'intelligence:sceneconfig:portrait', '上传', '/Sceneconfig/portrait', '10', '1');
INSERT INTO `cnvp_sys_res` VALUES ('187', '1', '下载头像', 'portraitdownload', 'start:portraitdownload', '下载头像', '/portrait_download', '10', '1');

-- ----------------------------
-- Table structure for cnvp_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_role`;
CREATE TABLE `cnvp_sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色Id',
  `cname` varchar(20) NOT NULL COMMENT '角色名称',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级Id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_sys_role
-- ----------------------------
INSERT INTO `cnvp_sys_role` VALUES ('1', '普通用户', '0');
INSERT INTO `cnvp_sys_role` VALUES ('4', '系统管理员', '0');

-- ----------------------------
-- Table structure for cnvp_sys_role_res
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_role_res`;
CREATE TABLE `cnvp_sys_role_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `res_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1723 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_sys_role_res
-- ----------------------------
INSERT INTO `cnvp_sys_role_res` VALUES ('1641', '4', '144');
INSERT INTO `cnvp_sys_role_res` VALUES ('1640', '4', '141');
INSERT INTO `cnvp_sys_role_res` VALUES ('1639', '4', '148');
INSERT INTO `cnvp_sys_role_res` VALUES ('1638', '4', '135');
INSERT INTO `cnvp_sys_role_res` VALUES ('1637', '4', '134');
INSERT INTO `cnvp_sys_role_res` VALUES ('1636', '4', '133');
INSERT INTO `cnvp_sys_role_res` VALUES ('1635', '4', '132');
INSERT INTO `cnvp_sys_role_res` VALUES ('1634', '4', '131');
INSERT INTO `cnvp_sys_role_res` VALUES ('1633', '4', '130');
INSERT INTO `cnvp_sys_role_res` VALUES ('1632', '4', '129');
INSERT INTO `cnvp_sys_role_res` VALUES ('1631', '4', '128');
INSERT INTO `cnvp_sys_role_res` VALUES ('1630', '4', '127');
INSERT INTO `cnvp_sys_role_res` VALUES ('1629', '4', '126');
INSERT INTO `cnvp_sys_role_res` VALUES ('1628', '4', '125');
INSERT INTO `cnvp_sys_role_res` VALUES ('1627', '4', '124');
INSERT INTO `cnvp_sys_role_res` VALUES ('1626', '4', '123');
INSERT INTO `cnvp_sys_role_res` VALUES ('1625', '4', '122');
INSERT INTO `cnvp_sys_role_res` VALUES ('1624', '4', '121');
INSERT INTO `cnvp_sys_role_res` VALUES ('1623', '4', '120');
INSERT INTO `cnvp_sys_role_res` VALUES ('1622', '4', '119');
INSERT INTO `cnvp_sys_role_res` VALUES ('1621', '4', '118');
INSERT INTO `cnvp_sys_role_res` VALUES ('1620', '4', '117');
INSERT INTO `cnvp_sys_role_res` VALUES ('1619', '4', '116');
INSERT INTO `cnvp_sys_role_res` VALUES ('1618', '4', '115');
INSERT INTO `cnvp_sys_role_res` VALUES ('1617', '4', '114');
INSERT INTO `cnvp_sys_role_res` VALUES ('1616', '4', '113');
INSERT INTO `cnvp_sys_role_res` VALUES ('1615', '4', '112');
INSERT INTO `cnvp_sys_role_res` VALUES ('1614', '4', '111');
INSERT INTO `cnvp_sys_role_res` VALUES ('1613', '4', '110');
INSERT INTO `cnvp_sys_role_res` VALUES ('1612', '4', '109');
INSERT INTO `cnvp_sys_role_res` VALUES ('1611', '4', '108');
INSERT INTO `cnvp_sys_role_res` VALUES ('1610', '4', '107');
INSERT INTO `cnvp_sys_role_res` VALUES ('1609', '4', '106');
INSERT INTO `cnvp_sys_role_res` VALUES ('1608', '4', '105');
INSERT INTO `cnvp_sys_role_res` VALUES ('1607', '4', '104');
INSERT INTO `cnvp_sys_role_res` VALUES ('1606', '4', '103');
INSERT INTO `cnvp_sys_role_res` VALUES ('1605', '4', '102');
INSERT INTO `cnvp_sys_role_res` VALUES ('1604', '4', '101');
INSERT INTO `cnvp_sys_role_res` VALUES ('1603', '4', '100');
INSERT INTO `cnvp_sys_role_res` VALUES ('1602', '4', '99');
INSERT INTO `cnvp_sys_role_res` VALUES ('1601', '4', '98');
INSERT INTO `cnvp_sys_role_res` VALUES ('1600', '4', '97');
INSERT INTO `cnvp_sys_role_res` VALUES ('1599', '4', '96');
INSERT INTO `cnvp_sys_role_res` VALUES ('1598', '4', '95');
INSERT INTO `cnvp_sys_role_res` VALUES ('1597', '4', '93');
INSERT INTO `cnvp_sys_role_res` VALUES ('1596', '4', '94');
INSERT INTO `cnvp_sys_role_res` VALUES ('1595', '4', '91');
INSERT INTO `cnvp_sys_role_res` VALUES ('1594', '4', '90');
INSERT INTO `cnvp_sys_role_res` VALUES ('1593', '4', '89');
INSERT INTO `cnvp_sys_role_res` VALUES ('1592', '4', '88');
INSERT INTO `cnvp_sys_role_res` VALUES ('1591', '4', '146');
INSERT INTO `cnvp_sys_role_res` VALUES ('1590', '4', '86');
INSERT INTO `cnvp_sys_role_res` VALUES ('1589', '4', '85');
INSERT INTO `cnvp_sys_role_res` VALUES ('1588', '4', '84');
INSERT INTO `cnvp_sys_role_res` VALUES ('1587', '4', '83');
INSERT INTO `cnvp_sys_role_res` VALUES ('1586', '4', '82');
INSERT INTO `cnvp_sys_role_res` VALUES ('1585', '4', '81');
INSERT INTO `cnvp_sys_role_res` VALUES ('1584', '4', '80');
INSERT INTO `cnvp_sys_role_res` VALUES ('1583', '4', '79');
INSERT INTO `cnvp_sys_role_res` VALUES ('1582', '4', '78');
INSERT INTO `cnvp_sys_role_res` VALUES ('1581', '4', '77');
INSERT INTO `cnvp_sys_role_res` VALUES ('1580', '4', '76');
INSERT INTO `cnvp_sys_role_res` VALUES ('1579', '4', '75');
INSERT INTO `cnvp_sys_role_res` VALUES ('1578', '4', '74');
INSERT INTO `cnvp_sys_role_res` VALUES ('1577', '4', '73');
INSERT INTO `cnvp_sys_role_res` VALUES ('1576', '4', '72');
INSERT INTO `cnvp_sys_role_res` VALUES ('1575', '4', '71');
INSERT INTO `cnvp_sys_role_res` VALUES ('1574', '4', '70');
INSERT INTO `cnvp_sys_role_res` VALUES ('1573', '4', '69');
INSERT INTO `cnvp_sys_role_res` VALUES ('1572', '4', '68');
INSERT INTO `cnvp_sys_role_res` VALUES ('1571', '4', '67');
INSERT INTO `cnvp_sys_role_res` VALUES ('1570', '4', '66');
INSERT INTO `cnvp_sys_role_res` VALUES ('1569', '4', '65');
INSERT INTO `cnvp_sys_role_res` VALUES ('1568', '4', '64');
INSERT INTO `cnvp_sys_role_res` VALUES ('1567', '4', '63');
INSERT INTO `cnvp_sys_role_res` VALUES ('1566', '4', '62');
INSERT INTO `cnvp_sys_role_res` VALUES ('1565', '4', '61');
INSERT INTO `cnvp_sys_role_res` VALUES ('1564', '4', '60');
INSERT INTO `cnvp_sys_role_res` VALUES ('1563', '4', '59');
INSERT INTO `cnvp_sys_role_res` VALUES ('1562', '4', '58');
INSERT INTO `cnvp_sys_role_res` VALUES ('1561', '4', '57');
INSERT INTO `cnvp_sys_role_res` VALUES ('1560', '4', '56');
INSERT INTO `cnvp_sys_role_res` VALUES ('1559', '4', '55');
INSERT INTO `cnvp_sys_role_res` VALUES ('1558', '4', '54');
INSERT INTO `cnvp_sys_role_res` VALUES ('1557', '4', '53');
INSERT INTO `cnvp_sys_role_res` VALUES ('1556', '4', '52');
INSERT INTO `cnvp_sys_role_res` VALUES ('1555', '4', '51');
INSERT INTO `cnvp_sys_role_res` VALUES ('1554', '4', '50');
INSERT INTO `cnvp_sys_role_res` VALUES ('1553', '4', '49');
INSERT INTO `cnvp_sys_role_res` VALUES ('1552', '4', '48');
INSERT INTO `cnvp_sys_role_res` VALUES ('1551', '4', '47');
INSERT INTO `cnvp_sys_role_res` VALUES ('1550', '4', '3');
INSERT INTO `cnvp_sys_role_res` VALUES ('1650', '4', '157');
INSERT INTO `cnvp_sys_role_res` VALUES ('1649', '4', '156');
INSERT INTO `cnvp_sys_role_res` VALUES ('1648', '4', '155');
INSERT INTO `cnvp_sys_role_res` VALUES ('1647', '4', '154');
INSERT INTO `cnvp_sys_role_res` VALUES ('1646', '4', '153');
INSERT INTO `cnvp_sys_role_res` VALUES ('1645', '4', '152');
INSERT INTO `cnvp_sys_role_res` VALUES ('1644', '4', '151');
INSERT INTO `cnvp_sys_role_res` VALUES ('1643', '4', '150');
INSERT INTO `cnvp_sys_role_res` VALUES ('1642', '4', '149');
INSERT INTO `cnvp_sys_role_res` VALUES ('1549', '4', '2');
INSERT INTO `cnvp_sys_role_res` VALUES ('1548', '4', '1');
INSERT INTO `cnvp_sys_role_res` VALUES ('1681', '1', '48');
INSERT INTO `cnvp_sys_role_res` VALUES ('1682', '1', '171');
INSERT INTO `cnvp_sys_role_res` VALUES ('1683', '1', '152');
INSERT INTO `cnvp_sys_role_res` VALUES ('1684', '1', '153');
INSERT INTO `cnvp_sys_role_res` VALUES ('1685', '1', '154');
INSERT INTO `cnvp_sys_role_res` VALUES ('1691', '4', '171');
INSERT INTO `cnvp_sys_role_res` VALUES ('1690', '4', '162');
INSERT INTO `cnvp_sys_role_res` VALUES ('1689', '1', '94');
INSERT INTO `cnvp_sys_role_res` VALUES ('1686', '1', '155');
INSERT INTO `cnvp_sys_role_res` VALUES ('1687', '1', '156');
INSERT INTO `cnvp_sys_role_res` VALUES ('1688', '1', '157');
INSERT INTO `cnvp_sys_role_res` VALUES ('1662', '1', '100');
INSERT INTO `cnvp_sys_role_res` VALUES ('1663', '1', '101');
INSERT INTO `cnvp_sys_role_res` VALUES ('1664', '1', '102');
INSERT INTO `cnvp_sys_role_res` VALUES ('1665', '1', '103');
INSERT INTO `cnvp_sys_role_res` VALUES ('1666', '1', '104');
INSERT INTO `cnvp_sys_role_res` VALUES ('1679', '1', '146');
INSERT INTO `cnvp_sys_role_res` VALUES ('1669', '1', '163');
INSERT INTO `cnvp_sys_role_res` VALUES ('1670', '4', '163');
INSERT INTO `cnvp_sys_role_res` VALUES ('1671', '4', '165');
INSERT INTO `cnvp_sys_role_res` VALUES ('1672', '1', '165');
INSERT INTO `cnvp_sys_role_res` VALUES ('1673', '1', '166');
INSERT INTO `cnvp_sys_role_res` VALUES ('1674', '4', '169');
INSERT INTO `cnvp_sys_role_res` VALUES ('1675', '1', '169');
INSERT INTO `cnvp_sys_role_res` VALUES ('1676', '1', '170');
INSERT INTO `cnvp_sys_role_res` VALUES ('1677', '4', '170');
INSERT INTO `cnvp_sys_role_res` VALUES ('1692', '1', '93');
INSERT INTO `cnvp_sys_role_res` VALUES ('1693', '1', '106');
INSERT INTO `cnvp_sys_role_res` VALUES ('1694', '1', '107');
INSERT INTO `cnvp_sys_role_res` VALUES ('1695', '1', '108');
INSERT INTO `cnvp_sys_role_res` VALUES ('1696', '1', '109');
INSERT INTO `cnvp_sys_role_res` VALUES ('1697', '1', '98');
INSERT INTO `cnvp_sys_role_res` VALUES ('1698', '1', '125');
INSERT INTO `cnvp_sys_role_res` VALUES ('1699', '1', '126');
INSERT INTO `cnvp_sys_role_res` VALUES ('1700', '1', '127');
INSERT INTO `cnvp_sys_role_res` VALUES ('1701', '1', '174');
INSERT INTO `cnvp_sys_role_res` VALUES ('1702', '1', '131');
INSERT INTO `cnvp_sys_role_res` VALUES ('1703', '1', '132');
INSERT INTO `cnvp_sys_role_res` VALUES ('1704', '1', '133');
INSERT INTO `cnvp_sys_role_res` VALUES ('1705', '1', '130');
INSERT INTO `cnvp_sys_role_res` VALUES ('1706', '1', '175');
INSERT INTO `cnvp_sys_role_res` VALUES ('1707', '1', '176');
INSERT INTO `cnvp_sys_role_res` VALUES ('1708', '1', '177');
INSERT INTO `cnvp_sys_role_res` VALUES ('1709', '1', '91');
INSERT INTO `cnvp_sys_role_res` VALUES ('1710', '1', '162');
INSERT INTO `cnvp_sys_role_res` VALUES ('1711', '1', '81');
INSERT INTO `cnvp_sys_role_res` VALUES ('1712', '4', '178');
INSERT INTO `cnvp_sys_role_res` VALUES ('1713', '4', '179');
INSERT INTO `cnvp_sys_role_res` VALUES ('1714', '1', '178');
INSERT INTO `cnvp_sys_role_res` VALUES ('1715', '1', '179');
INSERT INTO `cnvp_sys_role_res` VALUES ('1716', '1', '185');
INSERT INTO `cnvp_sys_role_res` VALUES ('1717', '1', '184');
INSERT INTO `cnvp_sys_role_res` VALUES ('1718', '1', '1');
INSERT INTO `cnvp_sys_role_res` VALUES ('1720', '1', '186');
INSERT INTO `cnvp_sys_role_res` VALUES ('1721', '1', '167');
INSERT INTO `cnvp_sys_role_res` VALUES ('1722', '1', '187');

-- ----------------------------
-- Table structure for cnvp_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_user`;
CREATE TABLE `cnvp_sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '账号',
  `account` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `sex` enum('男','女') DEFAULT NULL COMMENT '性别',
  `cname` varchar(10) DEFAULT NULL COMMENT '中文名',
  `ename` varchar(50) DEFAULT NULL COMMENT '英文名',
  `dept_id` int(11) DEFAULT NULL COMMENT '所在部门',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `flg` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `last_login_time` bigint(13) DEFAULT NULL COMMENT '最后登录时间',
  `create_time` bigint(13) NOT NULL COMMENT '创建时间',
  `create_user_id` int(11) NOT NULL COMMENT '创建者',
  `update_time` bigint(13) DEFAULT NULL COMMENT '更新时间',
  `update_user_id` int(11) DEFAULT NULL COMMENT '更新者',
  `email` varchar(512) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of cnvp_sys_user
-- ----------------------------
INSERT INTO `cnvp_sys_user` VALUES ('1', 'superadmin', '21232f297a57a5a743894a0e4a801fc3', '男', '超级第', 'uioyrew', '14', '12345678987', '1', null, '60', '1413779275', '0', '1', '1425458171011', '1', 'bb', '20160119115621059.jpg', 'oMkrew4ECscG0p8dvr177VpDIS_U');
INSERT INTO `cnvp_sys_user` VALUES ('9', '200057', 'a8f5f167f44f4964e6c998dee827110c', '男', '章宵', 'Michael', '14', '15990061612', '1', null, '0', null, '1425707342830', '1', '1425783657455', '1', null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('14', 'testUser2', '5f4dcc3b5aa765d61d8327deb882cf99', null, 'xiaowang', 'enname', '1', null, '1', 'comments', '0', null, '1441897818366', '1', '1442111724972', '1', null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('13', 'testuser1', '63a9f0ea7bb98050796b649e85481845', null, 'asdf', 'asdf', '1', null, '1', 'asdf', '0', null, '1441896820230', '1', null, null, null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('18', 'testUser2', '5f4dcc3b5aa765d61d8327deb882cf99', null, 'cnname', 'enname', null, null, '1', 'comments', '0', null, '1442109981314', '1', null, null, null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('17', 'testUser2', '5f4dcc3b5aa765d61d8327deb882cf99', null, 'cnname', 'enname', null, null, '1', 'comments', '0', null, '1442109911547', '1', null, null, null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('19', 'testUser2', '5f4dcc3b5aa765d61d8327deb882cf99', null, 'cnname', 'enname', null, null, '1', 'comments', '0', null, '1442110080179', '1', null, null, null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('20', 'testUser2', '5f4dcc3b5aa765d61d8327deb882cf99', null, 'cnname', 'enname', null, null, '1', 'comments', '0', null, '1442111724710', '1', null, null, null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('21', '123', '202cb962ac59075b964b07152d234b70', '男', '测试', 'test', '1', '1313', '1', null, '0', null, '1444307478723', '1', null, null, '123@qq.com', null, null);
INSERT INTO `cnvp_sys_user` VALUES ('23', 'test123', 'e10adc3949ba59abbe56e057f20f883e', '男', '测试', 'test', '1', '1313', '1', null, '0', null, '1444307577482', '1', null, null, '123@qq.com', null, null);
INSERT INTO `cnvp_sys_user` VALUES ('27', 'admin11', 'e10adc3949ba59abbe56e057f20f883e', '男', null, null, null, null, '1', null, '0', null, '1444804924728', '1', null, null, null, '', null);
INSERT INTO `cnvp_sys_user` VALUES ('28', 'admin12', 'e10adc3949ba59abbe56e057f20f883e', '男', null, null, null, null, '1', null, '0', null, '1444807753301', '1', null, null, null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('29', 'admin13', 'e10adc3949ba59abbe56e057f20f883e', '男', null, null, null, null, '1', null, '0', null, '1444808300339', '1', null, null, null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('30', 'admin14', 'e10adc3949ba59abbe56e057f20f883e', '男', null, null, null, null, '1', null, '0', null, '1444809047124', '1', null, null, null, null, null);
INSERT INTO `cnvp_sys_user` VALUES ('75', '123456', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, null, '1', null, '0', null, '1453441101609', '1', null, null, '123456', '504139779', null);
INSERT INTO `cnvp_sys_user` VALUES ('76', 'qwerty', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, null, '1', null, '0', null, '1453444333863', '1', null, null, '12345623234', '504139779', null);
INSERT INTO `cnvp_sys_user` VALUES ('77', 'wangjianrong', 'e3ceb5881a0a1fdaad01296d7554868d', '男', null, null, null, null, '1', null, '0', null, '1453445025229', '1', null, null, null, '504139779', null);
INSERT INTO `cnvp_sys_user` VALUES ('78', 'hushuting', 'c33367701511b4f6020ec61ded352059', null, null, null, null, null, '1', null, '0', null, '1453445140599', '1', null, null, '000000', '5DF83108-9B0D-46F0-98DA-DC4080A4B1BE', null);
INSERT INTO `cnvp_sys_user` VALUES ('73', 'linyousong', 'c33367701511b4f6020ec61ded352059', null, null, null, null, null, '1', null, '0', null, '1453440245754', '1', null, null, 'wfwefwef', '7B6A0C57-64D1-4427-A22B-C14C8AE108EE', null);
INSERT INTO `cnvp_sys_user` VALUES ('74', 'lysongzi', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, null, '1', null, '0', null, '1453440363334', '1', null, null, '324235', '4ACBDEA2-27DC-4E52-90C8-7379D9FB63B3', null);
INSERT INTO `cnvp_sys_user` VALUES ('72', 'gujinyue', 'c33367701511b4f6020ec61ded352059', null, null, null, null, null, '1', null, '0', null, '1453440208588', '1', null, null, '361143552@qq.com', '5A1241F8-59A4-42A0-B9AE-053AC1FC25D7', null);
INSERT INTO `cnvp_sys_user` VALUES ('79', '12345', '827ccb0eea8a706c4c34a16891f84e7b', null, null, null, null, null, '1', null, '0', null, '1453521667617', '1', null, null, '12345', '504139779', null);
INSERT INTO `cnvp_sys_user` VALUES ('80', '123456789', '25f9e794323b453885f5181f1b624d0b', null, null, null, null, null, '1', null, '0', null, '1453522108200', '1', null, null, '111', '504139779', null);
INSERT INTO `cnvp_sys_user` VALUES ('81', 'ce222', 'c33367701511b4f6020ec61ded352059', '男', null, null, null, null, '1', null, '0', null, '1453528618729', '1', null, null, null, '504139779', null);
INSERT INTO `cnvp_sys_user` VALUES ('82', 'zzz', 'f3abb86bd34cf4d52698f14c0da1dc60', null, null, null, null, null, '1', null, '0', null, '1453535938379', '1', null, null, 'zzz', '504139779', null);

-- ----------------------------
-- Table structure for cnvp_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_user_role`;
CREATE TABLE `cnvp_sys_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_sys_user_role
-- ----------------------------
INSERT INTO `cnvp_sys_user_role` VALUES ('1', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('1', '4');
INSERT INTO `cnvp_sys_user_role` VALUES ('4', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('9', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('10', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('10', '4');
INSERT INTO `cnvp_sys_user_role` VALUES ('13', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('14', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('17', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('18', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('19', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('20', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('27', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('29', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('30', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('32', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('33', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('34', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('35', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('36', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('37', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('38', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('39', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('40', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('41', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('42', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('43', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('44', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('45', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('46', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('47', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('48', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('49', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('50', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('51', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('52', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('53', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('54', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('55', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('56', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('57', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('58', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('59', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('60', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('61', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('62', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('63', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('64', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('65', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('66', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('67', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('68', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('69', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('70', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('71', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('72', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('73', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('74', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('75', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('76', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('77', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('78', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('79', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('80', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('81', '1');
INSERT INTO `cnvp_sys_user_role` VALUES ('82', '1');

-- ----------------------------
-- Table structure for cnvp_test
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_test`;
CREATE TABLE `cnvp_test` (
  `id` int(11) NOT NULL DEFAULT '0',
  `price` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of cnvp_test
-- ----------------------------

-- ----------------------------
-- Table structure for cnvp_test2
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_test2`;
CREATE TABLE `cnvp_test2` (
  `price` float DEFAULT NULL,
  `name` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_test2
-- ----------------------------

-- ----------------------------
-- Table structure for cnvp_test3
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_test3`;
CREATE TABLE `cnvp_test3` (
  `price` float DEFAULT NULL,
  `name` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_test3
-- ----------------------------

-- ----------------------------
-- Table structure for cnvp_user_room
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_user_room`;
CREATE TABLE `cnvp_user_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `delete` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cnvp_user_room
-- ----------------------------
