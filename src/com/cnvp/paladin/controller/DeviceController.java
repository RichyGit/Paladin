
package com.cnvp.paladin.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.shiro.SecurityUtils;
import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Devicecontrol;
import com.cnvp.paladin.model.Equipment;
import com.cnvp.paladin.model.SysUser;
import com.cnvp.paladin.model.SysUserRole;


public class DeviceController extends BaseController {

    boolean flag = false;

    /**
     * if the user is superadmin
     * 
     * @return return the userID
     */
    public int issuperadmin() {
        SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
        List<SysUserRole> sur = currentUser.getRoles();
        for (SysUserRole sr : sur) {
            if (sr.getInt("role_id") == 4)
                flag = true;
        }
        return userID;
    }

    /**
     * 
     * @param status_code
     *            describe the status_code such as 0,305,300
     * @param data
     * @param status
     * @return
     */
    public Map<String, Object> putjson(Object status_code, Object data, Object status) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("code", status_code);
        json.put("data", data);
        json.put("msg", status);
        return json;

    }

    public String sql() {
        String findSql = "(1=1) and ";
        Equipment eq = getModel(Equipment.class, "equipment");
        String[] attrs = eq.getAttrNames();
        Object[] values = eq.getAttrValues();
        for (int i = 0; i < attrs.length; i++) {
            System.out.println(attrs[i] + values[i]);
            Object a = null;
            if (null != values[i]) {
                a = values[i].getClass();
                findSql += (attrs[i] + "=" + values[i] + " and ");
            }
        }
        System.out.println("******************" + findSql + "******************");
        return findSql;
    }

    public void index() {
        setAttr("page", Equipment.dao.paginate(getParaToInt(0, 1), 10));
        int userID = issuperadmin();
        if (flag == true) {
            setAttr("page", Equipment.dao.paginate(getParaToInt(0, 1), 10));
        } else {
            String sql = "where user_id =" + userID;
            setAttr("page", Equipment.dao.paginate(getParaToInt(0, 1), 10, sql));
        }
    }

    public void getStatus(){
    	String is_app = getPara("is_app");
    	String logic_id = getPara("logic_id");
    	Equipment eq = Equipment.dao.find("select * from cnvp_equipment where logic_id = '"+logic_id+"'").get(0); 	
    	String status = eq.getStr("status");
    	if(status!=null){
    		if ("1".equals(is_app)) {
                renderJson(putjson("0", status, "success"));
            } else {
                redirect(getControllerKey());
            }
            return;
    	} else {
    		renderJson(putjson("301", "", "failed"));
    	}
    	
    }
    
    public void control(){
    	String is_app = getPara("is_app");
    	String logic_id = getPara("logic_id");
    	String action = getPara("action");
    	Equipment eq = Equipment.dao.find("select * from cnvp_equipment where logic_id = '"+logic_id+"'").get(0); 	
    	String status = eq.getStr("status");
    	if(status!=null&&status.equals(action)){
    		 if ("1".equals(is_app)) {
                 renderJson(putjson("0", "", "success"));
             } else {
                 redirect(getControllerKey());
             }
             return;
    	} else {
    		Devicecontrol dc = getModel(Devicecontrol.class,"devicecontrol");
        	dc.set("id", logic_id);
        	dc.set("command_type", "execute");
        	dc.set("action", action);
        	if(action.equals("open"))
        		dc.set("value", 100);
        	else
        		dc.set("value", 0);
        	if(dc.save()){
        		eq.set("status", action);
        		eq.update();
        		 if ("1".equals(is_app)) {
                     renderJson(putjson("0", "", "success"));
                 } else {
                     redirect(getControllerKey());
                 }
                 return;
        	}   		
        	else
        		renderJson(putjson("301", "", "failed"));
    	}
    }
    
}
