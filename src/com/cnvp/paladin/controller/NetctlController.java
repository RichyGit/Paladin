package com.cnvp.paladin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Equipment;
import com.cnvp.paladin.model.Netcontroller;
import com.cnvp.paladin.model.Phymapping;
import com.cnvp.paladin.model.SysDept;
import com.cnvp.paladin.model.SysUser;

public class NetctlController extends BaseController {

	public void index() {
		setAttr("page", Netcontroller.dao.paginate(getParaToInt(0, 1), 10));
	}

	public void find() {
		Map<String, Object> json = new HashMap<String, Object>();
		if (isPost()) {
			int id = getParaToInt("netcontroller.id") == null ? -1 : getParaToInt("netcontroller.id");
			SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
            int userID = currentUser.get("id");
			String ip_address = getPara("netcontroller.ip_address");
			int port = getParaToInt("netcontroller.port") == null ? -1 : getParaToInt("netcontroller.port");
			String main = getPara("netcontroller.main");
			String address = getPara("netcontroller.address");
			String domain = getPara("netcontroller.domain");
			String net_select = getPara("netcontroller.net_select");

			String findSql = "(1=1) and ";
			if(userID == 1){
			    if (id != -1)
	                findSql += "id=" + id + " and ";
	            if (ip_address!=null&&!ip_address.isEmpty())
	                findSql += "ip_address like '%" + ip_address + "%'" + " and ";
	            if (port != -1)
	                findSql += "port=" + port + " and ";
	            if (main!=null&&!main.isEmpty())
	                findSql += "main like '%" + main + "%'" + " and ";
	            if (address!=null&&!address.isEmpty())
	                findSql += "address like '%" + address + "%'" + " and ";
	            if (domain!=null&&!domain.isEmpty())
	                findSql += "domain like '%" + domain + "%'" + " and ";
	            if (net_select!=null&&!net_select.isEmpty())
	                findSql += "net_select like '%" + net_select + "%'" + " and ";
			}else{
			    if (id != -1)
	                findSql += "id=" + id + " and ";
	                findSql += "user_id=" + userID + " and ";
	            if (ip_address!=null&&!ip_address.isEmpty())
	                findSql += "ip_address like '%" + ip_address + "%'" + " and ";
	            if (port != -1)
	                findSql += "port=" + port + " and ";
	            if (main!=null&&!main.isEmpty())
	                findSql += "main like '%" + main + "%'" + " and ";
	            if (address!=null&&!address.isEmpty())
	                findSql += "address like '%" + address + "%'" + " and ";
	            if (domain!=null&&!domain.isEmpty())
	                findSql += "domain like '%" + domain + "%'" + " and ";
	            if (net_select!=null&&!net_select.isEmpty())
	                findSql += "net_select like '%" + net_select + "%'" + " and ";
			}
			
			findSql += " (1=1);";

			List<Netcontroller> data = Netcontroller.dao.where(findSql);
			if (data.size() != 0) {
				json.put("code", "0");
				json.put("data", data);
				json.put("msg", "success");
			} else {
				json.put("code", "305");
				json.put("data", data);
				json.put("msg", "返回数据为空");
			}
		}
		renderJson(json);
	}

	public void getlist() {
		Map<String, Object> json = new HashMap<String, Object>();
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
        if(userID == 1){
            List<Netcontroller> data = Netcontroller.dao.where("");
            json.put("code", "0");
            json.put("data", data);
            json.put("msg", "success");
        }else{
            String findSql = "(1=1) and ";
            findSql += "user_id=" + userID + " and ";
            findSql += " (1=1);";
            List<Netcontroller> data = Netcontroller.dao.where(findSql);
            json.put("code", "0");
            json.put("data", data);
            json.put("msg", "success");
        }
		
		renderJson(json);
	}

	public void create() {
		String is_app = getPara("is_app");
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
		Netcontroller nc = getModel(Netcontroller.class, "netcontroller");
		nc.set("user_id", userID);
		if (isPost()) {
			if (null == nc.get("ip_address") || null == nc.get("port")) {
				renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
			} else {
				nc.save();
				if ("1".equals(is_app)) {
					renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"success\"}");
				} else {
					redirect(getControllerKey());
				}
				return;
			}
		} else {
			Netcontroller data = new Netcontroller();
			setAttr("data", data);
			render("form.html");
		}
	}

	public void update() {
		String is_app = getPara("is_app");
		if (isPost()) {
			String flag = getPara("netcontroller.id");
			int id = 0;
			if (flag==null&&getParaToInt()!=null) {
				id = getParaToInt();
			} else if(!flag.isEmpty()){
				id = getParaToInt("netcontroller.id");
			}
			// 检查ID是否存在
			Netcontroller ncl = Netcontroller.dao.findById(id);
			if (ncl == null) {
				renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			} else {
				// 检验是否非空字段全部填写
				Netcontroller nc = getModel(Netcontroller.class, "netcontroller").set("id", id);
				if (null == nc.get("ip_address") || null == nc.get("port")) {
					renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
				} else {
					if (nc.update()) {
						if ("1".equals(is_app)) {
							renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"修改网关成功\"}");
						} else {
							redirect(getControllerKey());
						}
						return;
					}
				}
			}
		} else {
//			renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
			setAttr("data", Netcontroller.dao.findById(getParaToInt()));
			render("form.html");
		}
	}

	public void delete() {
		String is_app = getPara("is_app");
		String flag = getPara("netcontroller.id");
		int id = 0;
		boolean f=false;
		if(flag==null){
			f=true;
		}
		if (f&&getParaToInt()!=null) {
			id = getParaToInt();
		} else if(getParaToInt("netcontroller.id")!=null){
			id = getParaToInt("netcontroller.id");
		}
		Netcontroller nc = Netcontroller.dao.findById(id);
		if (nc != null) {
			nc.delete();
			if ("1".equals(is_app)) {
				renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"删除网关成功\"}");
			} else {
				redirect(getControllerKey());
			}
		} else {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		}
	}

	public void deleteAll() {
		String is_app = getPara("is_app");
		Integer[] ids = null;	
		if(!getPara("netcontroller.id").isEmpty()){
			ids = getParaValuesToInt("netcontroller.id");
		}else{
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			return;
		}
		boolean flag = true;
		for (Integer id : ids) {
			Netcontroller nc=Netcontroller.dao.findById(id);
			if (nc!=null) {
				nc.delete();}
			else{
				flag = false;
			}
		}
		if ("1".equals(is_app)&& flag == true) {
			renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"批量删除网关成功\"}");
		} else if ("1".equals(is_app) && flag == false) {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		} else {
			redirect(getControllerKey());
		}
	}

}
