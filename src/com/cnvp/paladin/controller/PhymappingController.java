package com.cnvp.paladin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Phymapping;
import com.cnvp.paladin.model.SysUser;;

public class PhymappingController extends BaseController {

	public void index() {
		setAttr("page", Phymapping.dao.paginate(getParaToInt(0, 1), 10));
	}

	public void find() {
		Map<String, Object> json = new HashMap<String, Object>();
		if (isPost()) {
			int id = getParaToInt("phymapping.id") == null ? -1 : getParaToInt("phymapping.id");
			SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
	        int userID = currentUser.get("id");
			int loc_id = getParaToInt("phymapping.loc_id") == null ? -1 : getParaToInt("phymapping.loc_id");
			String phy_addr = getPara("phymapping.phy_addr");
			String mac_addr = getPara("phymapping.mac_addr");
			String type = getPara("phymapping.type");
			int route = getParaToInt("phymapping.route") == null ? -1 : getParaToInt("phymapping.route");
			String tag = getPara("phymapping.tag");
			String describe = getPara("phymapping.describe");
			String status = getPara("phymapping.status");

			String findSql = "(1=1) and ";
			if(userID == 1){
			    if (id != -1)
	                findSql += "id=" + id + " and ";
	            if (loc_id != -1)
	                findSql += "loc_id like '%" + loc_id + "%'" + " and ";
	            if (phy_addr!=null&&!phy_addr.isEmpty())
	                findSql += "phy_addr like '%" + phy_addr + "%'" + " and ";
	            if (mac_addr!=null&&!mac_addr.isEmpty())
	                findSql += "mac_addr like '%" + mac_addr + "%'" + " and ";
	            if (type!=null&&!type.isEmpty())
	                findSql += "type like '%" + type + "%'" + " and ";
	            if (route != -1)
	                findSql += "route like '%" + route + "%'" + " and ";
	            if (tag!=null&&!tag.isEmpty())
	                findSql += "tag like '%" + tag + "%'" + " and ";
	            if (describe!=null&&!describe.isEmpty())
	                findSql += "cnvp_phymapping.describe like '%" + describe + "%'" + " and ";
	            if (status!=null&&!status.isEmpty())
	                findSql += "cnvp_phymapping.status like '%" + status + "%'" + " and ";
			}else{
			    if (id != -1)
	                findSql += "id=" + id + " and ";
	                findSql += "user_id=" + userID + " and ";
	            if (loc_id != -1)
	                findSql += "loc_id like '%" + loc_id + "%'" + " and ";
	            if (phy_addr!=null&&!phy_addr.isEmpty())
	                findSql += "phy_addr like '%" + phy_addr + "%'" + " and ";
	            if (mac_addr!=null&&!mac_addr.isEmpty())
	                findSql += "mac_addr like '%" + mac_addr + "%'" + " and ";
	            if (type!=null&&!type.isEmpty())
	                findSql += "type like '%" + type + "%'" + " and ";
	            if (route != -1)
	                findSql += "route like '%" + route + "%'" + " and ";
	            if (tag!=null&&!tag.isEmpty())
	                findSql += "tag like '%" + tag + "%'" + " and ";
	            if (describe!=null&&!describe.isEmpty())
	                findSql += "cnvp_phymapping.describe like '%" + describe + "%'" + " and ";
	            if (status!=null&&!status.isEmpty())
	                findSql += "cnvp_phymapping.status like '%" + status + "%'" + " and ";
			}
			findSql += " (1=1);";

			List<Phymapping> data = Phymapping.dao.where(findSql);
			// 正确情况
			if (data.size() != 0) {
				json.put("code", "0");
				json.put("data", data);
				json.put("msg", "success");
			} else {
				// 无返回数据的情况
				json.put("code", "305");
				json.put("data", data);
				json.put("msg", "返回数据为空");
			}
		}
		renderJson(json);
	}

	public void getlist() {
		Map<String, Object> json = new HashMap<String, Object>();
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
        if(userID == 1){
            List<Phymapping> data = Phymapping.dao.where("");
            json.put("code", "0");
            json.put("data", data);
            json.put("msg", "success");
        }else{
            String findSql = "(1=1) and ";
            findSql += "user_id=" + userID + " and ";
            findSql += " (1=1);";
            List<Phymapping> data = Phymapping.dao.where(findSql);
            json.put("code", "0");
            json.put("data", data);
            json.put("msg", "success");
        }
		
		renderJson(json);
	}

	public void create() {
		String is_app = getPara("is_app");
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
		if (isPost()) {
			// 检验是否非空字段全部填写
			Phymapping pm = getModel(Phymapping.class, "phymapping");
			pm.set("user_id", userID);
			if (null == pm.get("loc_id") || null == pm.get("mac_addr") || null == pm.get("type")) {
				renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
			} else {
				pm.save();
				if ("1".equals(is_app)) {
					renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"success\"}");
				} else {
					redirect(getControllerKey());
				}
				return;
			}
		} else {
			Phymapping data = new Phymapping();
			setAttr("data", data);
			render("form.html");
		}
	}

	public void update() {
		String is_app = getPara("is_app");		
		if (isPost()) {
			String flag = getPara("phymapping.id");
			int id = 0;
			if (flag==null&&getParaToInt()!=null) {
				id = getParaToInt();
			} else if(!flag.isEmpty()){
				id = getParaToInt("phymapping.id");
			}
			// 检查ID是否存在
			Phymapping pmp = Phymapping.dao.findById(id);
			if (pmp == null) {
				renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			} else {
				// 检验是否非空字段全部填写
				Phymapping pm = getModel(Phymapping.class, "phymapping").set("id", id);
				if (null == pm.get("loc_id") || null == pm.get("mac_addr") || null == pm.get("type")) {
					renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
				} else {
					if (pm.update()) {
						if ("1".equals(is_app)) {
							renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"修改物理设备成功\"}");
						} else {
							redirect(getControllerKey());
						}
						return;
					}
				}
			}
		} else {
//			renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
			setAttr("data", Phymapping.dao.findById(getParaToInt()));
			render("form.html");
		}
	}

	public void delete() {
		String is_app = getPara("is_app");
		String flag = getPara("phymapping.id");
		int id = 0;
		boolean f=false;
		if(flag==null){
			f=true;
		}
		if (f&&getParaToInt()!=null) {
			id = getParaToInt();
		} else if(getParaToInt("phymapping.id")!=null){
			id = getParaToInt("phymapping.id");
		}
		// 判断该id是否存在
		Phymapping pm = Phymapping.dao.findById(id);
		if (pm != null) {
			pm.delete();
			if ("1".equals(is_app)) {
				renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"删除物理设备成功\"}");
			} else {
				redirect(getControllerKey());
			}
		} else {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		}
	}

	public void deleteAll() {
		String is_app = getPara("is_app");
		Integer[] ids = null;	
		if(!getPara("phymapping.id").isEmpty()){
			ids = getParaValuesToInt("phymapping.id");
		}else{
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			return;
		}
		boolean flag = true;
		for (Integer id : ids) {
			Phymapping pm = Phymapping.dao.findById(id);
			if (pm != null) {
				pm.delete();
			} else {
				flag = false;
			}
		}
		if ("1".equals(is_app) && flag == true) {
			renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"批量删除物理设备成功\"}");
		} else if ("1".equals(is_app) && flag == false) {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		} else {
			redirect(getControllerKey());
		}
	}
}
