package com.cnvp.paladin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.shiro.SecurityUtils;
import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Equipment;
import com.cnvp.paladin.model.Room;
import com.cnvp.paladin.model.Rule;
import com.cnvp.paladin.model.SysUser;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class RuleController extends BaseController {

	public void index() {
		// setAttr("page", Rule.dao.paginate(getParaToInt(0, 1), 10));
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
		int userID = currentUser.get("id");
		if (userID == 1) {
			setAttr("page", Rule.dao.paginate(getParaToInt(0, 1), 10));
		} else {
			String sql = "where user_id =" + userID + " and `delete`=0";
			setAttr("page", Rule.dao.paginate(getParaToInt(0, 1), 10, sql));
		}
	}

	public void getEquipment() {
		setAttr("page", Equipment.dao.paginate(getParaToInt(0, 1), 10));
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
		int userID = currentUser.get("id");
		if (userID == 1) {
			setAttr("page", Equipment.dao.paginate(getParaToInt(0, 1), 10));
		} else {
			String sql = "where user_id =" + userID;
			setAttr("page", Equipment.dao.paginate(getParaToInt(0, 1), 10, sql));
		}
	}

	private static int id = 0;

	public void create() {
		String is_app = getPara("is_app");
		Rule r = getModel(Rule.class, "rule");
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
		int userID = currentUser.get("id");
		if (isPost()) {
			String msgtype = getPara("msgtype");
			String logic_id = getPara("logic_id");
			String epl = "select * from " + msgtype + "( ";
			String epla = null;
			String sql = "desc cnvp_" + msgtype;
			List<Record> column = Db.find(sql);
			for (int i = 0; i < column.size(); i++) {
				Record rd = column.get(i);
				String type = rd.getStr("type");
				String attr = getPara(rd.getStr("field"));
				if (type.contains("char")) {
					if (attr != null && !attr.isEmpty()) {
						epla = rd.getStr("field") + " = \"" + attr + "\"";
						epl += epla + " and ";
					}
				} else {
					String attrbefore = getPara(rd.getStr("field") + "before");
					if (attrbefore != null && !attrbefore.isEmpty()) {
						epla = rd.getStr("field") + " >= " + attrbefore;
						epl += epla + " and ";
						if (attr != null && !attr.isEmpty()) {
							epla = rd.getStr("field") + " <= " + attr;
							epl += epla + " and ";
						}
					}
				}

			}
			epl += "(1=1))";
			r.set("msgtype", msgtype);
			r.set("rule", epl);
			r.set("user_id", userID);
			String action = getPara("action");
			r.set("action", action);
			r.save();
			if ("1".equals(is_app)) {
				renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"success\"}");
			} else {
				redirect(getControllerKey());
			}
			return;
		} else {
			id = getParaToInt();
			Rule data = new Rule();
			setAttr("data", data);
			render("form.html");
		}
	}

	public void update() {
		String is_app = getPara("is_app");
		if (isPost()) {
			String flag = getPara("rule.id");
			int id = 0;
			if (flag == null && getParaToInt() != null) {
				id = getParaToInt();
			} else if (!flag.isEmpty()) {
				id = getParaToInt("rule.id");
			}
			Rule rl = Rule.dao.findById(id);
			if (rl == null) {
				renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			} else {
				// 检验是否非空字段全部填写
				Rule r = getModel(Rule.class, "rule").set("id", id);
				if (r.update()) {
					if ("1".equals(is_app)) {
						renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"修改房间成功\"}");
					} else {
						redirect(getControllerKey());
					}
					return;
				}
			}
		} else {
			setAttr("data", Rule.dao.findById(getParaToInt()));
			render("form.html");
		}
	}

	public void delete() {
		String is_app = getPara("is_app");
		String flag = getPara("rule.id");
		int id = 0;
		boolean f = false;
		if (flag == null) {
			f = true;
		}
		if (f && getParaToInt() != null) {
			id = getParaToInt();
		} else if (getParaToInt("rule.id") != null) {
			id = getParaToInt("rule.id");
		}
		Rule r = Rule.dao.findById(id);
		if (r != null) {
			r.set("delete", "YES");
			r.update();
			// r.delete();
			if ("1".equals(is_app)) {
				renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"删除房间成功\"}");
			} else {
				redirect(getControllerKey());
			}
		} else {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		}
	}

	public void deleteAll() {
		String is_app = getPara("is_app");
		Integer[] ids = null;
		if (!getPara("room.id").isEmpty()) {
			ids = getParaValuesToInt("room.id");
		} else {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			return;
		}
		boolean flag = true;
		for (Integer id : ids) {
			Room r = Room.dao.findById(id);
			if (r != null) {
				r.delete();
			} else {
				flag = false;
			}
		}
		if ("1".equals(is_app) && flag == true) {
			renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"批量删除房间成功\"}");
		} else if ("1".equals(is_app) && flag == false) {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		} else {
			redirect(getControllerKey());
		}
	}

	public void getMessageType() {
		Equipment eq = Equipment.dao.findById(id);
		String msgtype = eq.getStr("devicetype");
		String logic_id = eq.getStr("logic_id");
		String sql = "desc cnvp_" + msgtype;
		List<String> row = new ArrayList<String>();
		row.add(msgtype);
		row.add(logic_id);
		List<Record> column = Db.find(sql);
		for (int i = 0; i < column.size(); i++) {
			Record r = column.get(i);
			String type = r.getStr("type");
			String field = r.getStr("field");
			if(field.equals("logic_id"))
				continue;
			if (type.contains("char"))
				type = "<label for=\"sitename\"> = </label></div>";
			else {
				type = "<label for=\"sitename\"> >= </label></div><div class=\"field\"><input type=\"text\" id=\""
						+ field + "before\" name=\"" + field
						+ "before\" class=\"input input-auto\" size=\"50\"/></div></div>";
				type += "<div class=\"form-group\"><div class=\"label\"><label for=\"sitename\"> <= </label></div>";
			}
			row.add(field);
			row.add(type);
		}
		renderJavascript(JsonKit.toJson(row));
	}
}