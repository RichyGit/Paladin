package com.cnvp.paladin.controller;


import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Panel;
import com.cnvp.paladin.model.SysUser;

public class   PanelController extends BaseController {

	public void index() {
	    SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
		if (userID == 1){
		    setAttr("page", Panel.dao.paginate(getParaToInt(0, 1), 10));
		}else{
		    String sql = "where user_id =" + userID;
		    setAttr("page", Panel.dao.paginate(getParaToInt(0, 1), 10,sql));
		}
		   
		Method[] methods = Panel.class.getMethods();
		
		for (Method method : methods) {
			Class<?>[] types = method.getParameterTypes();
			
		}
		System.out.println( "methods===========" );
	}

	public void find() {
		Map<String, Object> json = new HashMap<String, Object>();
		if (isPost()) {
			int id = getParaToInt("panel.id") == null ? -1 : getParaToInt("panel.id");
			SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
            int userID = currentUser.get("id");
			String name = getPara("panel.name");
			String type = getPara("panel.type");

			String findSql = "(1=1) and ";
			if(userID == 1){
			    if (id != -1)
	                findSql += "id=" + id + " and ";
	            if (name!=null&&!name.isEmpty())
	                findSql += "name like '%" + name + "%'" + " and ";
	            if (type!=null&&!type.isEmpty())
	                findSql += "type like '%" + type + "%'" + " and ";
	            findSql += " (1=1);";
			}else{
			    if (id != -1)
	                findSql += "id=" + id + " and ";
	                findSql += "user_id=" + userID + " and ";
	            if (name!=null&&!name.isEmpty())
	                findSql += "name like '%" + name + "%'" + " and ";
	            if (type!=null&&!type.isEmpty())
	                findSql += "type like '%" + type + "%'" + " and ";
	            findSql += " (1=1);"; 
			}
			List<Panel> data = Panel.dao.where(findSql);
			if (!data.isEmpty()) {

				json.put("code", "0");
				json.put("data", data);
				json.put("msg", "success");

			} else {

				json.put("code", "305");
				json.put("data", data);
				json.put("msg", "返回数据为空");

			}

		}

		renderJson(json);
	}

	public void getlist() {
		Map<String, Object> json = new HashMap<String, Object>();
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
        if(userID == 1){
            List<Panel> data = Panel.dao.where("");
            json.put("code", "0");
            json.put("data", data);
            json.put("msg", "success");
        }else{
            String findSql = "(1=1) and ";
            findSql += "user_id=" + userID + " and ";
            findSql += " (1=1);";

            List<Panel> data = Panel.dao.where(findSql);
            json.put("code", "0");
            json.put("data", data);
            json.put("msg", "success");
        }
		renderJson(json);
	}

	public void create() {
		String is_app = getPara("is_app");
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
		int userID = currentUser.get("id");
		if (isPost()) {
			
			//检验参数是否为空  panel是传入的对象
			Panel panel=getModel(Panel.class, "panel");
			panel.set("user_id", userID);
			
			if(panel.get("name")==null||panel.get("type")==null)
			{
				renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
				
			}
			else{
                
				if(panel.save()){
					
					if ("1".equals(is_app)) {
						renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"success\"}");
					} else {
						redirect(getControllerKey());
					}
				
				}
				
			} 
				
			return;
			
		}else	{
				Panel data = new Panel();
				setAttr("data", data);
				render("form.html");		
		}	
	}
	
	public void update(){
		String is_app = getPara("is_app");				
		if(isPost()){
			String flag = getPara("panel.id");		
			int id = 0;
			if (flag.isEmpty()&&getParaToInt()!=null) {
				id = getParaToInt();
			} else if(!flag.isEmpty()){
				id = getParaToInt("panel.id");			
			}
			// 检验是否非空字段全部填写

				Panel panel = Panel.dao.findById(id);

				if(panel==null){
					
					renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
				}
				
				else{
					
					//检验字段是否为空   panel是传过来的参数   set加入id属性
					
					Panel panel1=getModel(Panel.class,"panel").set("id",id);
					
					if(panel1.get("name")==null||panel1.get("type")==null)
					{
						renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
					}
					
					else{
						
						if(panel1.update())
						if("1".equals(is_app)){
							renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"修改面板成功\"}");
						}else{
						redirect(getControllerKey());}
						
					}
					return;
					
				}
		}

		else{
//			renderJson("{\"code\":\"301\",\"msg\":\"缺少参数\"}");
		setAttr("data", Panel.dao.findById(getParaToInt()));
		render("form.html");
		
		}
	}
	
	
	public void delete() {
		String is_app = getPara("is_app");
		String flag = getPara("panel.id");
		int id = 0;
		boolean f=false;
		if(flag==null){
			f=true;
		}
		if (f&&getParaToInt()!=null) {
			id = getParaToInt();
		} else if(getParaToInt("panel.id")!=null){
			id = getParaToInt("panel.id");
		}

		Panel panel = Panel.dao.findById(id);
		if (panel != null) {
			panel.delete();
			if ("1".equals(is_app)) {				 
					renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"删除面板成功\"}");
				} else {
					redirect(getControllerKey());
				}

			}else {
				renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			}

		}		

	public void deleteAll() {
		String is_app = getPara("is_app");
		Integer[] ids = null;
		if(!getPara("panel.id").isEmpty()){
			ids = getParaValuesToInt("panel.id");
		}else{
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			return;
		}		
		boolean flag=true;				
		for (Integer id : ids) {
			Panel panel= Panel.dao.findById(id);
			
			if(panel!=null)
			{
				panel.delete();
			}else{
				
				flag=false;
			
			}
			
		}
		if ("1".equals(is_app)&flag==true) {
			renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"批量删除面板成功\"}");
		}else if ("1".equals(is_app) && flag == false) {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		}
		else {
			redirect(getControllerKey());
		}
	}

}
