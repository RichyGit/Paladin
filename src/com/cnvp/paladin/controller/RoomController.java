package com.cnvp.paladin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Equipment;
import com.cnvp.paladin.model.Phymapping;
import com.cnvp.paladin.model.Room;
import com.cnvp.paladin.model.SysUser;

public class RoomController extends BaseController {

	public void index() {
		setAttr("page", Room.dao.paginate(getParaToInt(0, 1), 10));
	}

	public void find() {
		Map<String, Object> json = new HashMap<String, Object>();
		if (isPost()) {
			int id = getParaToInt("room.id") == null ? -1 : getParaToInt("room.id");
			SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
	        int userID = currentUser.get("id");
			String name = getPara("room.name");
			String type = getPara("room.type");
			String city = getPara("room.city");
			String environment = getPara("room.environment");
			String building = getPara("room.building");
			int floor = getParaToInt("room.floor") == null ? -1 : getParaToInt("room.floor");
			String num_in_floor = getPara("room.num_in_floor");

			String findSql = "(1=1) and ";
			if(userID == 1){
			    if (id != -1)
	                findSql += "id=" + id + " and ";
	            if (name!=null&&!name.isEmpty())
	                findSql += "name like '%" + name + "%'" +  " and ";
	            if (type!=null&&!type.isEmpty())
	                findSql += "type like '%" + type + "%'" + " and ";
	            if (city!=null&&!city.isEmpty())
	                findSql += "city like '%" + city + "%'" + " and ";
	            if (environment!=null&&!environment.isEmpty())
	                findSql += "environment like '%" + environment + "%'" + " and ";
	            if (building!=null&&!building.isEmpty())
	                findSql += "building like '%" + building + "%'" + " and ";
	            if (floor != -1)
	                findSql += "floor=" + floor + " and ";
	            if (num_in_floor!=null&&!num_in_floor.isEmpty())
	                findSql += "num_in_floor like '%" + num_in_floor + "%'" + " and ";
			}else{
			    if (id != -1)
	                findSql += "id=" + id + " and ";
	                findSql += "user_id=" + userID + " and ";
	            if (name!=null&&!name.isEmpty())
	                findSql += "name like '%" + name + "%'" +  " and ";
	            if (type!=null&&!type.isEmpty())
	                findSql += "type like '%" + type + "%'" + " and ";
	            if (city!=null&&!city.isEmpty())
	                findSql += "city like '%" + city + "%'" + " and ";
	            if (environment!=null&&!environment.isEmpty())
	                findSql += "environment like '%" + environment + "%'" + " and ";
	            if (building!=null&&!building.isEmpty())
	                findSql += "building like '%" + building + "%'" + " and ";
	            if (floor != -1)
	                findSql += "floor=" + floor + " and ";
	            if (num_in_floor!=null&&!num_in_floor.isEmpty())
	                findSql += "num_in_floor like '%" + num_in_floor + "%'" + " and ";
			}
			
			findSql += " (1=1);";

			List<Room> data = Room.dao.where(findSql);
			if (data.size() != 0) {
				json.put("code", "0");
				json.put("data", data);
				json.put("msg", "success");
			} else {
				json.put("code", "305");
				json.put("data", data);
				json.put("msg", "返回数据为空");
			}
		}
		renderJson(json);
	}

	public void getlist() {
		Map<String, Object> json = new HashMap<String, Object>();
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
        if(userID == 1){
            List<Room> data = Room.dao.where("");
            json.put("code", "0");
            json.put("data", data);
            json.put("msg", "success");
        }else{
            String findSql = "(1=1) and ";
            findSql += "user_id=" + userID + " and ";
            findSql += " (1=1);";
            List<Room> data = Room.dao.where(findSql);
            json.put("code", "0");
            json.put("data", data);
            json.put("msg", "success");
        }
		
		renderJson(json);
	}

	public void create() {
		String is_app = getPara("is_app");
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        int userID = currentUser.get("id");
		Room r = getModel(Room.class, "room");
		r.set("user_id", userID);
		if (isPost()) {
			if (null == r.get("name") || null == r.get("type")) {
				renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
			} else {
				r.save();
				if ("1".equals(is_app)) {
					renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"success\"}");
				} else {
					redirect(getControllerKey());
				}
				return;
			}
		} else {
			Room data = new Room();
			setAttr("data", data);
			render("form.html");
		}
	}

	public void update() {
		String is_app = getPara("is_app");
		if (isPost()) {
			String flag = getPara("room.id");
			int id = 0;
			if (flag==null&&getParaToInt()!=null) {
				id = getParaToInt();
			} else if(!flag.isEmpty()){
				id = getParaToInt("room.id");
			}
			Room rm = Room.dao.findById(id);
			if (rm == null) {
				renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			} else {
				// 检验是否非空字段全部填写
				Room r = getModel(Room.class, "room").set("id", id);
				if (null == r.get("name") || null == r.get("type")) {
					renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
				} else {
					if (r.update()) {
						if ("1".equals(is_app)) {
							renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"修改房间成功\"}");
						} else {
							redirect(getControllerKey());
						}
						return;
					}
				}
			}
		} else {
//			renderJson("{\"code\":\"301\",\"data\":\"\",\"msg\":\"缺少参数\"}");
			setAttr("data", Room.dao.findById(getParaToInt()));
			render("form.html");
		}
	}

	public void delete() {
		String is_app = getPara("is_app");
		String flag = getPara("room.id");
		int id = 0;
		boolean f=false;
		if(flag==null){
			f=true;
		}
		if (f&&getParaToInt()!=null) {
			id = getParaToInt();
		} else if(getParaToInt("room.id")!=null){
			id = getParaToInt("room.id");
		}
		Room r=Room.dao.findById(id);
		if (r!=null) {
			r.delete();
			if ("1".equals(is_app)) {
				renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"删除房间成功\"}");
			} else {
				redirect(getControllerKey());
			}
		} else {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		}
	}

	public void deleteAll() {
		String is_app = getPara("is_app");
		Integer[] ids = null;	
		if(!getPara("room.id").isEmpty()){
			ids = getParaValuesToInt("room.id");
		}else{
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
			return;
		}
		boolean flag = true;
		for (Integer id : ids) {
			Room r=Room.dao.findById(id);
			if (r!=null) {
				r.delete();
			}
			else{
				flag = false;
			}
		}
		if ("1".equals(is_app)&& flag == true) {
			renderJson("{\"code\":\"0\",\"data\":\"\",\"msg\":\"批量删除房间成功\"}");
		} else if ("1".equals(is_app) && flag == false) {
			renderJson("{\"code\":\"307\",\"data\":\"\",\"msg\":\"该主键不存在\"}");
		}else {
			redirect(getControllerKey());
		}
	}

}