package com.cnvp.paladin.controller;

import java.io.File;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.SecurityUtils;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.SysUser;
import com.jfinal.upload.UploadFile;

/**
 * 手机接口类
 * 
 * @author WilliamLee
 * 
 */
public class AppController extends BaseController {

	// 获取服务器的路径
	URL url = IndexController.class.getResource("/");
	// 储存路径
	String getPath = url.getPath().substring(0, url.getPath().length() - 16);
	// 设置文件的路径
	String path = getPath + "Static/images/protrait/";

	/**
	 * 二进制文件存入服务器并且返回文件名
	 * 
	 * @author WilliamLee
	 * @date 2015年10月13日 下午6:39:05
	 */
	public void upload() {
		Map<String, Object> json = new HashMap<String, Object>();

		// 获取上传的文件
		UploadFile file = getFile("file");
		if (!file.getFile().exists()) {
			json.put("code", "309");
			json.put("msg", "文件不存在");
			renderJson(json);
			return;
		}
		String fileName = file.getFileName();
		String[] token = fileName.split("\\.");
		if (token.length > 1) {
			// 获取文件名的后缀
			String postfix = token[token.length - 1];
			// 获取当前时间
			Format format = new SimpleDateFormat("yyyyMMddhhmmssSSS");
			String fileNamebe = format.format(new Date());
			// 最终的文件名
			String finalName = fileNamebe + "." + postfix;

			String portrait = path + finalName;
			File localFile = file.getFile();
			localFile.renameTo(new File(portrait));
			json.put("data", "http://" + getRequest().getServerName() + ":" + getRequest().getServerPort()
					+ "/paladin/Static/images/protrait/" + finalName);
			json.put("msg", "success");
			renderJson(json);
			return;
		}
	}

	/**
	 * 更新用户头像URL，并且删除旧头像
	 * 
	 * @author WilliamLee
	 * @date 2015年10月15日 下午12:37:45
	 */
	public void updatePortrait() {
		Map<String, Object> json = new HashMap<String, Object>();
		SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
		currentUser = SysUser.dao.findById(currentUser.get("id"));
		File deleteFile = new File(path + currentUser.get("url"));
		String oldUrl = currentUser.get("url");
		String url = getPara("url");
		if (url != null) {
			String[] token = url.split("/");
			if (token.length > 1) {
				String postfix = token[token.length - 1];
				currentUser.set("url", postfix);
			} else
				currentUser.set("url", url);
			// 存在旧的头像并且旧头像不为默认头像
			if (deleteFile.exists() && !"default.jpg".equals(oldUrl))
				deleteFile.delete();
			currentUser.update();
			json.put("code", "0");
			json.put("msg", "success");
			renderJson(json);
			return;
		} else {
			json.put("code", "311");
			json.put("msg", "头像路径为空");
		}
	}
}
