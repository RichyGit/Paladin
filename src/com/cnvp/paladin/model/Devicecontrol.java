package com.cnvp.paladin.model;

import com.cnvp.paladin.core.BaseModel;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class Devicecontrol extends BaseModel<Devicecontrol> {
	private int isUpdate=1;
	public static final Devicecontrol dao = new Devicecontrol();
	public Page<Devicecontrol> paginate(int pageNumber, int pageSize) {
		return paginate(pageNumber, pageSize, "select *", "from "+getTableName()+" order by id asc");
	}
	public boolean hasChild(){
		if (Devicecontrol.dao.set("pid", get("id")).findByModel().size()>0) 
			return true;
		else
			return false;
	}
	public void getSelect(){
		
	}
}