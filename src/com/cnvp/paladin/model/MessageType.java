package com.cnvp.paladin.model;

import com.cnvp.paladin.core.BaseModel;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class MessageType extends BaseModel<MessageType> {
	public static final MessageType dao = new MessageType();
	public Page<MessageType> paginate(int pageNumber, int pageSize) {
		return paginate(pageNumber, pageSize, "select *", "from "+getTableName()+" order by id asc");
	}
	public Page<MessageType> paginate(int pageNumber, int pageSize,String sql) {
        return paginate(pageNumber, pageSize, "select *", "from "+getTableName()+" " + sql+" order by id asc");
    }
	public boolean hasChild(){
		if (MessageType.dao.set("pid", get("id")).findByModel().size()>0) 
			return true;
		else
			return false;
	}
	public void getSelect(){
		
	}
}