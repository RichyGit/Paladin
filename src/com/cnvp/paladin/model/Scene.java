package com.cnvp.paladin.model;

import com.cnvp.paladin.core.BaseModel;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class Scene extends BaseModel<Scene> {
	public static final Scene dao = new Scene();
	public Page<Scene> paginate(int pageNumber, int pageSize, String sql) {
		return paginate(pageNumber, pageSize, "select *", "from "+getTableName()+" order by id asc");
	}
	public boolean hasChild(){
		if (Scene.dao.set("pid", get("id")).findByModel().size()>0) 
			return true;
		else
			return false;
	}
	public void getSelect(){
		
	}
}