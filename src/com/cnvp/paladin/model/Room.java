package com.cnvp.paladin.model;

import com.cnvp.paladin.core.BaseModel;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class Room extends BaseModel<Room> {
	public static final Room dao = new Room();
	public Page<Room> paginate(int pageNumber, int pageSize) {
		return paginate(pageNumber, pageSize, "select *", "from "+getTableName()+" order by id asc");
	}
	public boolean hasChild(){
		if (Room.dao.set("pid", get("id")).findByModel().size()>0) 
			return true;
		else
			return false;
	}
	public void getSelect(){
		
	}
}