package com.cnvp.paladin.model;

import com.cnvp.paladin.core.BaseModel;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class Netcontroller extends BaseModel<Netcontroller> {
	public static final Netcontroller dao = new Netcontroller();
	public Page<Netcontroller> paginate(int pageNumber, int pageSize) {
		return paginate(pageNumber, pageSize, "select *", "from "+getTableName()+" order by id asc");
	}
	public boolean hasChild(){
		if (Netcontroller.dao.set("pid", get("id")).findByModel().size()>0) 
			return true;
		else
			return false;
	}
	public void getSelect(){
		
	}
}