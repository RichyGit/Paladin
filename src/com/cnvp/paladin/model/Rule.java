package com.cnvp.paladin.model;

import com.cnvp.paladin.core.BaseModel;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class Rule extends BaseModel<Rule> {
	public static final Rule dao = new Rule();
	public Page<Rule> paginate(int pageNumber, int pageSize) {
		return paginate(pageNumber, pageSize, "select *", "from "+getTableName()+" order by id asc");
	}
	public Page<Rule> paginate(int pageNumber, int pageSize,String sql) {
        return paginate(pageNumber, pageSize, "select *", "from "+getTableName()+" " + sql+" order by id asc");
    }
	public boolean hasChild(){
		if (Rule.dao.set("pid", get("id")).findByModel().size()>0) 
			return true;
		else
			return false;
	}
	public void getSelect(){
		
	}
}