package com.cnvp.paladin.utils.beetl.fn;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.BitSet;

public class URLEncoder {

   private static final String UTF8 = "UTF-8";
   private static final BitSet URI_UNRESERVED_CHARS = new BitSet(256);
   private static final BitSet LAZY_IGNORE_CHARS;
   private static final int caseDiff = 32;


   @Deprecated
   public static String pathEncode(String s) {
      try {
         return s == null?null:encodeSome(s, LAZY_IGNORE_CHARS);
      } catch (UnsupportedEncodingException var2) {
         throw new IllegalStateException("UTF-8 encoding is missing", var2);
      }
   }

   public static String uriEncode(String s) {
      try {
         return s == null?null:encodeSome(s, URI_UNRESERVED_CHARS);
      } catch (UnsupportedEncodingException var2) {
         throw new IllegalStateException("UTF-8 encoding is missing", var2);
      }
   }

   public static String encodeAll(String s) {
      try {
         return encodeSome(s, (BitSet)null);
      } catch (UnsupportedEncodingException var2) {
         throw new IllegalStateException("UTF-8 encoding is missing", var2);
      }
   }

   private static String encodeSome(String s, BitSet dontNeedEncoding) throws UnsupportedEncodingException {
      if(s == null) {
         return "";
      } else {
         boolean needToChange = false;
         boolean wroteUnencodedChar = false;
         byte maxBytesPerChar = 10;
         StringBuffer out = new StringBuffer(s.length());
         ByteArrayOutputStream buf = new ByteArrayOutputStream(maxBytesPerChar);
         OutputStreamWriter writer = new OutputStreamWriter(buf, "UTF-8");

         for(int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if(dontNeedEncoding != null && dontNeedEncoding.get(c)) {
               out.append((char)c);
               wroteUnencodedChar = true;
            } else {
               try {
                  if(wroteUnencodedChar) {
                     writer = new OutputStreamWriter(buf, "UTF-8");
                     wroteUnencodedChar = false;
                  }

                  writer.write(c);
                  if(c >= '\ud800' && c <= '\udbff' && i + 1 < s.length()) {
                     char ba = s.charAt(i + 1);
                     if(ba >= '\udc00' && ba <= '\udfff') {
                        writer.write(ba);
                        ++i;
                     }
                  }

                  writer.flush();
               } catch (IOException var13) {
                  buf.reset();
                  continue;
               }

               byte[] var14 = buf.toByteArray();

               for(int j = 0; j < var14.length; ++j) {
                  out.append('%');
                  char ch = Character.forDigit(var14[j] >> 4 & 15, 16);
                  if(Character.isLetter(ch)) {
                     ch = (char)(ch - 32);
                  }

                  out.append(ch);
                  ch = Character.forDigit(var14[j] & 15, 16);
                  if(Character.isLetter(ch)) {
                     ch = (char)(ch - 32);
                  }

                  out.append(ch);
               }

               buf.reset();
               needToChange = true;
            }
         }

         return needToChange?out.toString():s;
      }
   }

   public static String decode(String s) {
      try {
         return URLDecoder.decode(s, "UTF-8");
      } catch (UnsupportedEncodingException var2) {
         return URLDecoder.decode(s);
      }
   }

   static {
      int i;
      for(i = 97; i <= 122; ++i) {
         URI_UNRESERVED_CHARS.set(i);
      }

      for(i = 65; i <= 90; ++i) {
         URI_UNRESERVED_CHARS.set(i);
      }

      for(i = 48; i <= 57; ++i) {
         URI_UNRESERVED_CHARS.set(i);
      }

      URI_UNRESERVED_CHARS.set(45);
      URI_UNRESERVED_CHARS.set(46);
      URI_UNRESERVED_CHARS.set(95);
      URI_UNRESERVED_CHARS.set(126);
      LAZY_IGNORE_CHARS = (BitSet)URI_UNRESERVED_CHARS.clone();
      LAZY_IGNORE_CHARS.set(47);
   }
}

