
package com.cnvp.paladin.core;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.jfinal.plugin.activerecord.ICallback;

/**
 * @ClassName: MyCallback
 * @Description: TODO(执行提供的sql语句)
 * @author huqun
 * @date 2015年11月24日 下午8:26:31
 * 
 */
public class MyCallback implements ICallback {

	private String sql;

	public MyCallback(String sql) {
		this.sql = sql;
	}

	@Override
	public Object call(Connection conn) throws SQLException {
		// TODO Auto-generated method stub

		Statement sqlStatement = conn.createStatement();
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.execute();
		return null;
	}

}
